package com.grace.ui.customer.adapters;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.grace.R;
import com.grace.databinding.ListItemProvidersBinding;
import com.grace.interfaces.OnItemClickListener;
import com.grace.models.ServiceProviderListResponse;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ProviderAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final Context context;
    private List<ServiceProviderListResponse.DataBean> list;
    private OnItemClickListener listener;
    private int lastPosition;

    public ProviderAdapter(Context context, List<ServiceProviderListResponse.DataBean> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ListItemProvidersBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.list_item_providers, parent, false);
        return new ViewHolder(binding);
    }

    public void updateList(List<ServiceProviderListResponse.DataBean> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof ViewHolder) {
            ((ViewHolder) holder).bindData(list.get(holder.getAdapterPosition()));
        }

        Animation animation = AnimationUtils.loadAnimation(context,
                (position > lastPosition) ? R.anim.up_from_bottom
                        : R.anim.down_from_top);
        holder.itemView.startAnimation(animation);
        lastPosition = position;
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    @Override
    public int getItemCount() {
        return /*list.size()==0?15:*/list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ListItemProvidersBinding binding;
        OnItemClickListener viewListener;

        ViewHolder(ListItemProvidersBinding itemView) {
            super(itemView.getRoot());
            binding = itemView;
            binding.cvProviders.setOnClickListener(this);
        }

        private void setOnItemClickListener(OnItemClickListener listener) {
            viewListener = listener;
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.cv_providers:
                    viewListener.onItemClick(getAdapterPosition(), "");
                    break;
            }
        }

        void bindData(ServiceProviderListResponse.DataBean dataBean) {

            binding.tvSalonName.setText(dataBean.getFullName());
            binding.tvAddress.setText(dataBean.getAddress());
            binding.tvTime.setText(String.format("%s mins", dataBean.getTime()));
            binding.tvDistance.setText(String.format("%s miles", dataBean.getDistance()));

            if (!TextUtils.isEmpty(dataBean.getSalonInfo().getImage())) {
                binding.progress.setVisibility(View.VISIBLE);
                Picasso.get().load(dataBean.getSalonInfo().getImage()).resize(250,250).into(binding.ivSalon, new Callback() {
                    @Override
                    public void onSuccess() {
                        binding.progress.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError(Exception e) {
                        e.printStackTrace();
                        binding.progress.setVisibility(View.GONE);
                    }

                });
            }else{
                binding.ivSalon.setImageResource(R.drawable.ic_profile);
            }

            setOnItemClickListener(listener);
        }
    }

}
