package com.grace.ui.customer;


import android.app.ActivityOptions;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.grace.R;
import com.grace.base.BaseActivity;
import com.grace.databinding.FragmentCustomerProfileBinding;
import com.grace.models.UpdateProfileResponse;
import com.grace.models.UserDataResponse;
import com.grace.networks.APIHandler;
import com.grace.networks.APIResponseInterface;
import com.grace.ui.common.HolderActivity;
import com.grace.utils.Constants;
import com.grace.utils.FileUtils;
import com.grace.utils.PermissionsAndroid;
import com.grace.utils.Prefs;
import com.grace.utils.Utils;
import com.grace.utils.Validations;
import com.kbeanie.multipicker.api.CameraImagePicker;
import com.kbeanie.multipicker.api.ImagePicker;
import com.kbeanie.multipicker.api.Picker;
import com.kbeanie.multipicker.api.callbacks.ImagePickerCallback;
import com.kbeanie.multipicker.api.entity.ChosenImage;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class CustomerProfileFragment extends Fragment implements APIResponseInterface.OnCallableResponse,
        Constants.NAVIGATE_SCREENS, Constants.PARAMS, Constants.API_CALLS, ImagePickerCallback, View.OnClickListener {

    FragmentCustomerProfileBinding binding;


    private final APIHandler apiHandler;
    private String profileImagePath;
    private Place place;
    private int PLACE_PICKER_REQUEST = 1;
    private CameraImagePicker cameraPicker;
    private ImagePicker imagePicker;
    private String pickerPath;
    private boolean cameraSelected = false;
    private boolean gallerySelected = false;
    private boolean isImageSelected = false;
    private MenuItem save, edit;
    private String latitude = "", longitude = "";
    private boolean isDashboard = false;
    private String userID = "", role = "";

    public CustomerProfileFragment() {
        // Required empty public constructor
        apiHandler = new APIHandler(this);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            isDashboard = getArguments().getBoolean(getString(R.string.isDashboard), false);
            userID = getArguments().getString(USER_ID, "");
            role = getArguments().getString(ROLE, "");
        }

        if (isDashboard)
            setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_customer_profile, container, false);
        return binding.getRoot();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_profile, menu);
        edit = menu.findItem(R.id.action_edit);
        save = menu.findItem(R.id.action_save);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_edit:
                updateUI(true);
                edit.setVisible(false);
                save.setVisible(true);
                break;

            case R.id.action_save:
                updateUI(false);
                save.setVisible(false);
                edit.setVisible(true);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void updateUI(boolean isEdit) {
        if (isEdit) {
            binding.etFullName.setEnabled(true);
            binding.etAddress.setEnabled(true);
            binding.btnUpdate.setEnabled(true);
            binding.fabPicker.setEnabled(true);

            binding.fabPicker.setOnClickListener(this);
            binding.etAddress.setOnClickListener(this);
            binding.btnUpdate.setOnClickListener(this);
            binding.ivPic.setOnClickListener(this);


        } else {
            binding.etFullName.setEnabled(false);
            binding.etAddress.setEnabled(false);
            binding.btnUpdate.setEnabled(false);
            binding.fabPicker.setEnabled(false);

            binding.fabPicker.setOnClickListener(null);
            binding.etAddress.setOnClickListener(null);
            binding.btnUpdate.setOnClickListener(null);
            binding.ivPic.setOnClickListener(null);


        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        updateUI(true);

        setPrefData();

    }

    private void setPrefData() {
        binding.etEmail.setText(Prefs.getString(EMAIL, ""));
        binding.etPhone.setText(Prefs.getString(PHONE, ""));
        binding.etFullName.setText(Prefs.getString(FULL_NAME, ""));
        binding.etAddress.setText(Prefs.getString(ADDRESS, ""));
        latitude = Prefs.getString(LATITUDE, "");
        longitude = Prefs.getString(LONGITUDE, "");


        if (!TextUtils.isEmpty(Prefs.getString(SALON_IMAGE, ""))) {
            binding.progress.setVisibility(View.VISIBLE);
            Picasso.get().load(Prefs.getString(SALON_IMAGE, "")).resize(300,300).into(binding.ivPic, new Callback() {
                @Override
                public void onSuccess() {
                    binding.progress.setVisibility(View.GONE);
                }

                @Override
                public void onError(Exception e) {
                    e.printStackTrace();
                    binding.progress.setVisibility(View.GONE);
                }

            });
        }
    }

    @Override
    public void onSuccess(Response response, String api) {
        if (getActivity() != null) {
            if (((BaseActivity) getActivity()).isShowing())
                ((BaseActivity) getActivity()).hideProgressDialog();
            switch (api) {
                case UPDATE_PROFILE:
                    if (response.isSuccessful()) {
                        UpdateProfileResponse updateProfileResponse = (UpdateProfileResponse) response.body();
                        if (updateProfileResponse != null) {
                            UserDataResponse dataObj = updateProfileResponse.getData().get(0);
                            Prefs.putString(USER_ID, dataObj.getUserId());
                            Prefs.putString(EMAIL, dataObj.getEmail());
                            Prefs.putString(ROLE, dataObj.getRole());
                            Prefs.putString(PHONE, dataObj.getPhone());
                            Prefs.putString(EXPERTISE_ID, dataObj.getExpertise());
                            Prefs.putString(SALON_NAME, dataObj.getSalonName());
                            Prefs.putString(LATITUDE, String.valueOf(dataObj.getLatitude()));
                            Prefs.putString(LONGITUDE, String.valueOf(dataObj.getLongitude()));
                            Prefs.putString(SALON_IMAGE, dataObj.getSalonImage());
                            Prefs.putString(FULL_NAME, dataObj.getFullName());
                            Prefs.putString(ADDRESS, dataObj.getAddress());
                            Toast.makeText(getActivity(), updateProfileResponse.getMessage(), Toast.LENGTH_SHORT).show();


                            if (!isDashboard) {
                                startActivity(new Intent(getActivity(), CustomerDashboard.class));
                                getActivity().finish();
                            }else {
                                updateUI(false);
                                save.setVisible(false);
                                edit.setVisible(true);
                            }

                            if (getActivity() instanceof CustomerDashboard) {
                                ((CustomerDashboard) getActivity()).updateData();
                            }

                        }
                    }
                    break;
            }
        }
    }

    @Override
    public void onFailure(Throwable t, String api) {
        if (getActivity() != null) {
            Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            if (((BaseActivity) getActivity()).isShowing())
                ((BaseActivity) getActivity()).hideProgressDialog();
        }
    }

    @Override
    public void onClick(View v) {
        if (getActivity() != null) {
            switch (v.getId()) {
                case R.id.fab_picker:
                    // show picker for image capture
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    String[] options = {"Camera", "Gallery", "Cancel"};
                    final Bundle bundle = new Bundle();
                    builder.setTitle("Choose Option")
                            .setItems(options, (dialog, which) -> {
                                // The 'which' argument contains the index position
                                switch (which) {
                                    case 0:
                                        cameraSelected = true;
                                        gallerySelected = false;
                                        checkCameraPermission();
                                        break;
                                    case 1:
                                        cameraSelected = false;
                                        gallerySelected = true;
                                        checkCameraPermission();
                                        break;
                                    case 2:
                                        dialog.dismiss();
                                        break;
                                }
                            });
                    builder.setCancelable(false);
                    builder.show();
                    break;
                case R.id.btn_update:
                    updateProfileData();
                    break;

                case R.id.et_address:
                    //Move to Search Address Fragment
                    ((BaseActivity) getActivity()).showProgressDialog();
                    try {
                        PlacePicker.IntentBuilder placeBuilder = new PlacePicker.IntentBuilder();
                        getActivity().startActivityForResult(placeBuilder.build(getActivity()), PLACE_PICKER_REQUEST);
                    } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
                        e.printStackTrace();
                        ((BaseActivity) getActivity()).hideProgressDialog();
                    }
                    break;

                case R.id.iv_pic:
                    enlargeImage();
                    break;
            }
        }
    }


    private void enlargeImage() {
        if (getActivity() != null) {
            if (!TextUtils.isEmpty(profileImagePath)) {


                Intent profilePicIntent = new Intent(getActivity(), HolderActivity.class);
                profilePicIntent.putExtra(SCREEN, VIEW_IMAGE_SCREEN);
                profilePicIntent.putExtra(PIC_TITLE, "Profile Pic");
                profilePicIntent.putExtra(BROWSE_PIC, profileImagePath);
                if (Utils.getInstance().isEqualLollipop()) {
                    Pair<View, String> p1 = Pair.create(binding.ivPic, TRANSITION_IMAGE);
                    ActivityOptions options =
                            ActivityOptions.makeSceneTransitionAnimation(getActivity(), p1);
                    getActivity().startActivity(profilePicIntent, options.toBundle());
                } else {
                    getActivity().startActivity(profilePicIntent);
                }


            } else if (!TextUtils.isEmpty(Prefs.getString(SALON_IMAGE, ""))) {
                Intent profilePicIntent = new Intent(getActivity(), HolderActivity.class);
                profilePicIntent.putExtra(SCREEN, VIEW_IMAGE_SCREEN);
                profilePicIntent.putExtra(PIC_TITLE, "Profile Pic");
                profilePicIntent.putExtra(BROWSE_PIC, Prefs.getString(SALON_IMAGE, ""));
                if (Utils.getInstance().isEqualLollipop()) {
                    Pair<View, String> p1 = Pair.create(binding.ivPic, TRANSITION_IMAGE);
                    ActivityOptions options =
                            ActivityOptions.makeSceneTransitionAnimation(getActivity(), p1);
                    getActivity().startActivity(profilePicIntent, options.toBundle());
                } else {
                    getActivity().startActivity(profilePicIntent);
                }
            }
        }
    }


    private void updateProfileData() {
        if (getActivity() != null) {
            if (Utils.getInstance().isInternetAvailable(getActivity())) {
                String fullName = binding.etFullName.getText().toString().trim();
                String address = binding.etAddress.getText().toString().trim();
                String response = Validations.getInstance().validateCustomerProfileFields(address, fullName);
                if (!TextUtils.isEmpty(response)) {
                    Snackbar.make(binding.getRoot(), response, Snackbar.LENGTH_SHORT).show();
                } else {
                    ((BaseActivity) getActivity()).showProgressDialog();
                    Map<String, String> params = new HashMap<>();
                    params.put(USER_ID, userID);
                    params.put(ROLE, role);
                    params.put(FULL_NAME, fullName);
                    params.put(ADDRESS, address);
                    params.put(LATITUDE, latitude);
                    params.put(LONGITUDE, longitude);
                    if (isImageSelected && !TextUtils.isEmpty(profileImagePath)) {
                        apiHandler.updateProfileWithPic(params, profileImagePath, UPDATE_PROFILE);
                    } else {
                        apiHandler.updateProfile(params, UPDATE_PROFILE);
                    }
                }
            } else {
                Utils.getInstance().showSnackBar(getView(), getString(R.string.error_internet));
            }
        }
    }

    //  @TargetApi(Build.VERSION_CODES.M)
    private void checkCameraPermission() {
        boolean isExternalStorage = PermissionsAndroid.getInstance().checkCameraPermission(getActivity());
        if (!isExternalStorage) {
            PermissionsAndroid.getInstance().requestForCameraPermission(this);
        } else if (cameraSelected) {
            takePicture();
        } else if (gallerySelected) {
            pickImageSingle();
        }
    }

    private void takePicture() {
        cameraPicker = new CameraImagePicker(getActivity());
//        cameraPicker.setCacheLocation(CacheLocation.INTERNAL_APP_DIR);
        cameraPicker.setImagePickerCallback(this);
        cameraPicker.shouldGenerateMetadata(true);
        cameraPicker.shouldGenerateThumbnails(true);
        pickerPath = cameraPicker.pickImage();
    }

    private void pickImageSingle() {
        imagePicker = new ImagePicker(getActivity());
        imagePicker.shouldGenerateMetadata(true);
        imagePicker.shouldGenerateThumbnails(true);
        imagePicker.setImagePickerCallback(this);
        imagePicker.pickImage();
    }

    @Override
    public void onImagesChosen(List<ChosenImage> list) {
        if (getActivity() != null) {
            String extension = "";
            int i = list.get(0).getDisplayName().lastIndexOf('.');
            if (i > 0) {
                extension = list.get(0).getDisplayName().substring(i + 1);
            }
            if (!extension.equals("png") && !extension.equals("jpg") && !extension.equals("jpeg")) {
                ((BaseActivity) getActivity()).showSnackBar(getView(), "you can choose only images.");
                return;
            }
            ChosenImage image = list.get(0);
            isImageSelected = true;
            profileImagePath = FileUtils.getPath(getActivity(), Uri.parse(image.getQueryUri()));
            binding.progress.setVisibility(View.VISIBLE);
            Picasso.get().load(new File(profileImagePath)).resize(300,300).into(binding.ivPic, new Callback() {
                @Override
                public void onSuccess() {
                    binding.progress.setVisibility(View.GONE);
                }

                @Override
                public void onError(Exception e) {
                    binding.progress.setVisibility(View.GONE);
                }

            });
        }
    }

    @Override
    public void onError(String s) {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (getActivity() != null) {
            if (((BaseActivity) getActivity()).isShowing()) {
                ((BaseActivity) getActivity()).hideProgressDialog();
            }
            if (resultCode == RESULT_OK) {
                if (requestCode == PLACE_PICKER_REQUEST) {
                    place = PlacePicker.getPlace(getActivity(), data);
                    if (place != null) {
                        latitude = String.valueOf(place.getLatLng().latitude);
                        longitude = String.valueOf(place.getLatLng().longitude);
                        binding.etAddress.setText(place.getAddress());

                    }
                } else if (requestCode == Picker.PICK_IMAGE_DEVICE) {
                    if (imagePicker == null) {
                        imagePicker = new ImagePicker(getActivity());
                    }
                    imagePicker.submit(data);
                } else if (requestCode == Picker.PICK_IMAGE_CAMERA) {
                    if (cameraPicker == null) {
                        cameraPicker = new CameraImagePicker(getActivity());
                        cameraPicker.setImagePickerCallback(this);
                        cameraPicker.reinitialize(pickerPath);
                    }
                    cameraPicker.submit(data);
                }
            } else {
                Utils.getInstance().showToast("Request cancelled");
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case 102: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Show option to update profile
                    if (cameraSelected) {
                        takePicture();
                    } else if (gallerySelected) {
                        pickImageSingle();
                    }

                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Utils.getInstance().showToast("Camera/Storage permission Denied");
                }
                return;
            }

        }
    }

}
