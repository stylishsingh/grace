package com.grace.ui.customer;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.grace.R;
import com.grace.databinding.FragmentGreetBinding;
import com.grace.utils.Constants;
import com.grace.utils.Prefs;

/**
 * A simple {@link Fragment} subclass.
 */
public class GreetFragment extends Fragment {

    FragmentGreetBinding binding;

    public GreetFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_greet, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.tvGreet.append(" " + Prefs.getString(Constants.PARAMS.FULL_NAME, ""));

        binding.btnDone.setOnClickListener(v -> {
            if (getActivity() != null) {
                getActivity().onBackPressed();
            }
        });
    }
}
