package com.grace.ui.customer;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.grace.R;
import com.grace.base.BaseActivity;
import com.grace.databinding.FragmentConfirmationBinding;
import com.grace.models.ExpertiseResponse;
import com.grace.models.GenericResponse;
import com.grace.models.ServiceProviderListResponse;
import com.grace.networks.APIHandler;
import com.grace.networks.APIResponseInterface;
import com.grace.ui.common.HolderActivity;
import com.grace.utils.Constants;
import com.grace.utils.Prefs;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ConfirmationFragment extends Fragment implements APIResponseInterface.OnCallableResponse, Constants.PARAMS, Constants.API_CALLS, View.OnClickListener {

    FragmentConfirmationBinding binding;
    private APIHandler apiHandler;
    private String latitude = "", longitude = "", totalAmount = "", time = "", salonID = "", providerID = "",
            menQuantity = "", womenQuantity = "", kidsQuantity = "", serviceAddress = "", expertiseID = "", providerName = "";
    private ServiceProviderListResponse.DataBean.SalonInfoBean salonInfoBean;
    private ExpertiseResponse.DataBean expertiseBean;

    public ConfirmationFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_confirmation, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getActivity() != null && getContext() != null && getArguments() != null) {
            apiHandler = new APIHandler(this);


            ((HolderActivity) getActivity()).setSupportActionBar(binding.toolbar);
            if (((HolderActivity) getActivity()).getSupportActionBar() != null) {
                ((HolderActivity) getActivity()).getSupportActionBar().setTitle("");
            }


            binding.toolbar.setNavigationIcon(R.drawable.ic_back);

            binding.toolbar.setNavigationOnClickListener(v -> {
                if (getActivity() != null) {
                    getActivity().onBackPressed();
                }
            });

            latitude = getArguments().getString(LATITUDE);
            longitude = getArguments().getString(LONGITUDE);
            time = getArguments().getString(DATE);
            providerName = getArguments().getString(PROVIDER_NAME);
            totalAmount = getArguments().getString(TOTAL_AMOUNT);
            menQuantity = getArguments().getString(MEN);
            womenQuantity = getArguments().getString(WOMEN);
            kidsQuantity = getArguments().getString(KIDS);
            serviceAddress = getArguments().getString(ADDRESS);
            providerID = getArguments().getString(PROVIDER_ID);
            salonInfoBean = getArguments().getParcelable(SALON_INFO);
            expertiseBean = getArguments().getParcelable(EXPERTISE_LIST);

            binding.toolbar.setTitle("Confirmation");

            binding.toolbar.setNavigationOnClickListener(v -> {
                getActivity().onBackPressed();
            });


            setData();

            binding.tvBook.setOnClickListener(this);

        }
    }

    private void setData() {
        if (expertiseBean != null) {
            expertiseID = expertiseBean.getExpertiseId();
            binding.tvService.setText(expertiseBean.getName());
        }
        binding.tvDateTime.setText(time);
        binding.tvProvider.setText(providerName);
        binding.tvAddress.setText(serviceAddress);
        binding.tvAmount.setText("$" + totalAmount);
    }

    @Override
    public void onSuccess(Response response, String api) {
        binding.includeLoader.setVisibility(View.GONE);
        if (getActivity() != null) {
            switch (api) {
                case BOOK_APPOINTMENT:
                    if (response.isSuccessful()) {
                        GenericResponse genericResponse = (GenericResponse) response.body();
                        if (genericResponse != null) {
                            Snackbar.make(binding.getRoot(), genericResponse.getMessage(), Snackbar.LENGTH_SHORT).show();
                            switch (genericResponse.getStatus()) {
                                case Constants.STATUS_200:

                                    ((BaseActivity) getActivity()).clearBackStack();
                                    binding.tvBook.setVisibility(View.GONE);
                                    binding.cvBookingDetails.setVisibility(View.GONE);

                                    ((BaseActivity) getActivity()).navigateToWithBundle(R.id.container, new GreetFragment(), null, false, null);

                                    break;
                            }
                        }
                    }

                    break;
            }
        }
    }

    @Override
    public void onFailure(Throwable t, String api) {

    }

    private void bookAppointment() {
        if (getActivity() != null) {
            if (((BaseActivity) getActivity()).isInternetAvailable(getActivity())) {

                binding.includeLoader.setVisibility(View.VISIBLE);
                Map<String, String> params = new HashMap<>();
                params.put(USER_ID, Prefs.getString(USER_ID, ""));
                params.put(SERVICE_PROVIDER_ID, providerID);
                params.put(SALON_ID, salonInfoBean.getSalonId());
                params.put(TIME, time);
                params.put(TOTAL_AMOUNT, String.valueOf(totalAmount));
                params.put(MEN, menQuantity);
                params.put(WOMEN, womenQuantity);
                params.put(KIDS, kidsQuantity);
                params.put(LATITUDE, latitude);
                params.put(ADDRESS, serviceAddress);
                params.put(LONGITUDE, longitude);
                params.put(EXPERTISE_ID, expertiseID);
                apiHandler.bookAppointment(params, Constants.API_CALLS.BOOK_APPOINTMENT);
            } else {
                Toast.makeText(getActivity(), R.string.error_internet, Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_book:
                bookAppointment();
                break;
        }
    }
}
