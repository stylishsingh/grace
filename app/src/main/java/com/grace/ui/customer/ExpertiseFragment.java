package com.grace.ui.customer;


import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.grace.R;
import com.grace.base.BaseActivity;
import com.grace.databinding.FragmentSalonDetailBinding;
import com.grace.models.ExpertiseResponse;
import com.grace.networks.APIHandler;
import com.grace.networks.APIResponseInterface;
import com.grace.ui.common.HolderActivity;
import com.grace.ui.customer.adapters.ExpertiseAdapter;
import com.grace.utils.Constants;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ExpertiseFragment extends Fragment implements Constants.API_CALLS, Constants.PARAMS,
        APIResponseInterface.OnCallableResponse {

    private FragmentSalonDetailBinding binding;
    private LinearLayoutManager linearLayoutManager;

    private final APIHandler apiHandler;
    private ExpertiseAdapter adapter;
    private List<ExpertiseResponse.DataBean> list=new ArrayList<>();

    public ExpertiseFragment() {
        apiHandler = new APIHandler(this);
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_salon_detail, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getActivity() != null) {



            linearLayoutManager = new LinearLayoutManager(getActivity());
            binding.recyclerView.setLayoutManager(linearLayoutManager);
            adapter = new ExpertiseAdapter(getContext(), list);
            binding.recyclerView.setAdapter(adapter);
            getExpertises();

            adapter.setOnItemClickListener((layoutPosition, check) -> {
                startActivity(new Intent(getActivity(), HolderActivity.class)
                        .putExtra(Constants.NAVIGATE_SCREENS.SCREEN, Constants.NAVIGATE_SCREENS.BOOK_APPOINTMENT)
                        .putExtra(EXPERTISE_LIST, list.get(layoutPosition)));
            });
        }

    }


    @Override
    public void onSuccess(Response response, String api) {
        binding.includeLoader.setVisibility(View.GONE);
        if (getActivity() != null) {
            switch (api) {
                case EXPERTISE_LIST:
                    if (response.isSuccessful()) {
                        ExpertiseResponse expertiseResponse = (ExpertiseResponse) response.body();
                        if (expertiseResponse != null) {
                            Snackbar.make(binding.getRoot(), expertiseResponse.getMessage(), Snackbar.LENGTH_SHORT).show();
                            switch (expertiseResponse.getStatus()) {
                                case Constants.STATUS_200:
                                    list = expertiseResponse.getData();
                                    adapter.updateList(list);
                                    break;
                                    default:
                                        adapter.updateList(new ArrayList<>());
                            }
                        }else{
                            adapter.updateList(new ArrayList<>());
                        }
                    }

                    break;
            }
        }
    }

    @Override
    public void onFailure(Throwable t, String api) {
        if (getActivity() != null) {
            t.getLocalizedMessage();
            binding.includeLoader.setVisibility(View.GONE);
        }
    }

    private void getExpertises() {
        if (getActivity() != null) {
            if (((BaseActivity) getActivity()).isInternetAvailable(getActivity())) {
                binding.includeLoader.setVisibility(View.VISIBLE);
                apiHandler.getExpertise(Constants.API_CALLS.EXPERTISE_LIST);
            } else {
                Toast.makeText(getActivity(), R.string.error_internet, Toast.LENGTH_SHORT).show();
            }
        }
    }
}
