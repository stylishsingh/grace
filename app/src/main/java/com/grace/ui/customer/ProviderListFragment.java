package com.grace.ui.customer;


import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.Task;
import com.grace.R;
import com.grace.base.BaseActivity;
import com.grace.databinding.FragmentProviderListBinding;
import com.grace.models.ExpertiseResponse;
import com.grace.models.ServiceProviderListResponse;
import com.grace.networks.APIHandler;
import com.grace.networks.APIResponseInterface;
import com.grace.ui.common.HolderActivity;
import com.grace.ui.customer.adapters.ProviderAdapter;
import com.grace.utils.Constants;
import com.grace.utils.PermissionsAndroid;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Response;

import static android.support.constraint.Constraints.TAG;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProviderListFragment extends Fragment implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        APIResponseInterface.OnCallableResponse,
        Constants.API_CALLS, Constants.PARAMS, Constants.NAVIGATE_SCREENS {

    private FragmentProviderListBinding binding;

    LinearLayoutManager linearLayoutManager;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private ProviderAdapter adapter;
    private APIHandler apiHandler;
    private List<ServiceProviderListResponse.DataBean> list = new ArrayList<>();
    private String latitude = "", longitude = "";

    /**
     * Constant used in the location settings dialog.
     */
    private static final int REQUEST_CHECK_SETTINGS = 0x1;

    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;

    /**
     * The fastest rate for active location updates. Exact. Updates will never be more frequent
     * than this value.
     */
    private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS =
            UPDATE_INTERVAL_IN_MILLISECONDS / 2;


    private FusedLocationProviderClient mFusedLocationClient;
    private SettingsClient mSettingsClient;
    private LocationCallback mLocationCallback;
    private Bundle bundle;
    private Location mCurrentLocation;
    private LocationSettingsRequest mLocationSettingsRequest;
    private boolean mRequestingLocationUpdates;
    private ExpertiseResponse.DataBean expertiseBean;

    BroadcastReceiver gpsBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getBooleanExtra("isGPSEnabled", false)){

            }
//                checkLocationCameraPermission();
        }
    };
    private String expertiseID = "", expertiseName = "";

    public ProviderListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_provider_list, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getActivity() != null && getContext() != null && getArguments() != null) {
            bundle = getArguments();
            getActivity().registerReceiver(gpsBroadcastReceiver, new IntentFilter(Constants.GPS_RECEIVER));


            ((HolderActivity) getActivity()).setSupportActionBar(binding.toolbar);
            if (((HolderActivity) getActivity()).getSupportActionBar() != null)
                ((HolderActivity) getActivity()).getSupportActionBar().setTitle("");


            binding.toolbar.setNavigationIcon(R.drawable.ic_back);

            binding.toolbar.setNavigationOnClickListener(v -> {
                if (getActivity() != null)
                    getActivity().onBackPressed();
            });

            expertiseBean = getArguments().getParcelable(EXPERTISE_LIST);
            latitude = getArguments().getString(LATITUDE);
            longitude = getArguments().getString(LONGITUDE);

            expertiseBean = getArguments().getParcelable(EXPERTISE_LIST);

            if (expertiseBean != null)
                binding.toolbar.setTitle(expertiseBean.getName());

            initUI();


            apiHandler = new APIHandler(this);

            getProviderList(latitude,longitude);

            /*mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getContext());
            mSettingsClient = LocationServices.getSettingsClient(getContext());

            // Kick off the process of building the LocationCallback, LocationRequest, and
            // LocationSettingsRequest objects.
            buildGoogleApiClient();
            createLocationCallback();
            createLocationRequest();
            connectToGoogleApiClient();
            checkLocationCameraPermission();*/
        }

    }

    private void createLocationRequest() {
        mLocationRequest = new LocationRequest();

//        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);

//        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);

        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    /**
     * Creates a callback for receiving location events.
     */
    private void createLocationCallback() {
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);

                mCurrentLocation = locationResult.getLastLocation();
                latitude = String.valueOf(mCurrentLocation.getLatitude());
                longitude = String.valueOf(mCurrentLocation.getLongitude());
                binding.includeLoader.setVisibility(View.GONE);
                getProviderList(latitude, longitude);

            }
        };
    }

    /**
     * Uses a {@link com.google.android.gms.location.LocationSettingsRequest.Builder} to build
     * a {@link com.google.android.gms.location.LocationSettingsRequest} that is used for checking
     * if a device has the needed location settings.
     */
    private void buildLocationSettingsRequest() {
        if (getActivity() != null) {
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
            builder.addLocationRequest(mLocationRequest);
            mLocationSettingsRequest = builder.build();


            SettingsClient client = LocationServices.getSettingsClient(getActivity());
            Task<LocationSettingsResponse> task = client.checkLocationSettings(mLocationSettingsRequest);

            task.addOnSuccessListener(locationSettingsResponse -> {
                System.out.println("Task on Success");
                checkLocationCameraPermission();
            });

            task.addOnFailureListener(e -> {
                int statusCode = ((ApiException) e).getStatusCode();
                System.out.println("Task on Failure");
                switch (statusCode) {
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        Log.i(TAG, "Location settings are not satisfied. Attempting to upgrade " +
                                "location settings ");
                        try {
                            // Show the dialog by calling startResolutionForResult(), and check the
                            // result in onActivityResult().
                            ResolvableApiException rae = (ResolvableApiException) e;
                            rae.startResolutionForResult(getActivity(), REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException sie) {
                            Log.i(TAG, "PendingIntent unable to execute request.");
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        String errorMessage = "Location settings are inadequate, and cannot be " +
                                "fixed here. Fix in Settings.";
                        Log.e(TAG, errorMessage);
                        Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_LONG).show();
                        mRequestingLocationUpdates = false;
                }
            });
        }

    }


    private void initUI() {
        if (getActivity() != null) {
            linearLayoutManager = new LinearLayoutManager(getActivity());
            binding.recyclerView.setLayoutManager(linearLayoutManager);

            adapter = new ProviderAdapter(getContext(), list);
            adapter.setOnItemClickListener((layoutPosition, check) -> {
                bundle.putParcelable(SALON_INFO, list.get(layoutPosition).getSalonInfo());
                bundle.putString(PROVIDER_ID, list.get(layoutPosition).getUserId());
                bundle.putString(PROVIDER_NAME, list.get(layoutPosition).getFullName());
                ((BaseActivity) getActivity()).navigateToWithBundle(R.id.container, new ConfirmationFragment(), null, true, bundle);
            });
            binding.recyclerView.setAdapter(adapter);
        }


    }

    private void checkLocationCameraPermission() {
        if (!PermissionsAndroid.getInstance().checkLocationPermission(getActivity())) {
            // Do not have permissions, request them now
            PermissionsAndroid.getInstance().requestForLocationPermission(this);
            return;
        }
        mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                mLocationCallback, Looper.myLooper());
    }


    private void getProviderList(String latitude, String longitude) {
        if (getActivity() != null) {
            if (((BaseActivity) getActivity()).isInternetAvailable(getActivity())) {
                binding.includeLoader.setVisibility(View.VISIBLE);

                Map<String, String> params = new HashMap<>();
                params.put(LATITUDE, latitude);
                params.put(LONGITUDE, longitude);
                params.put(EXPERTISE_ID, expertiseBean.getExpertiseId());

                apiHandler.getProviderList(params, SERVICE_PROVIDER_LIST);
            } else {
                Toast.makeText(getActivity(), R.string.error_internet, Toast.LENGTH_SHORT).show();
            }
        }
    }

    /**
     * Builds a GoogleApiClient. Uses the {@code #addApi} method to request the LocationServices API.
     */
    private synchronized void buildGoogleApiClient() {
        if (getContext() != null)
            mGoogleApiClient = new GoogleApiClient.Builder(getContext())
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        buildLocationSettingsRequest();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PermissionsAndroid.LOCATION_PERMISSION_REQUEST_CODE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getActivity(), "Location Permission Granted", Toast.LENGTH_SHORT).show();
                    checkLocationCameraPermission();
                } else if (Build.VERSION.SDK_INT >= 23 && permissions.length > 0 && !shouldShowRequestPermissionRationale(permissions[0])) {
                    System.out.println("Go to settings>>>>>>");

                } else {
                    Toast.makeText(getActivity(), "Location Permission denied", Toast.LENGTH_SHORT).show();
                }
            }

        }
    }


    /**
     * function to connect to Google API Client
     */
    private void connectToGoogleApiClient() {
        if (mGoogleApiClient != null && !mGoogleApiClient.isConnected()) {
            mGoogleApiClient.connect();
        }
    }

    /**
     * function to disconnect from Google API Client
     */
    private void disconnectFromGoogleApiClient() {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        disconnectFromGoogleApiClient();

    }


    @Override
    public void onSuccess(Response response, String api) {
        switch (api) {
            case SERVICE_PROVIDER_LIST:
                binding.includeLoader.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    ServiceProviderListResponse providerListResponse = (ServiceProviderListResponse) response.body();
                    if (providerListResponse != null) {
                        switch (providerListResponse.getStatus()) {
                            case Constants.STATUS_200:
                                if (!providerListResponse.getData().isEmpty()) {
                                    binding.includeNoRecord.setVisibility(View.GONE);
                                    list = providerListResponse.getData();
                                    adapter.updateList(list);
                                } else {
                                    binding.includeNoRecord.setVisibility(View.VISIBLE);
                                    adapter.updateList(new ArrayList<>());
                                }
                                break;
                            case Constants.STATUS_201:
                                binding.includeNoRecord.setVisibility(View.VISIBLE);
                                adapter.updateList(new ArrayList<>());
                                break;
                        }

                    } else {
                        binding.includeNoRecord.setVisibility(View.VISIBLE);
                        adapter.updateList(new ArrayList<>());
                    }
                }


                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            // Check for the integer request code originally supplied to startResolutionForResult().
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        Log.i(TAG, "User agreed to make required location settings changes.");
                        // Nothing to do. startLocationupdates() gets called in onResume again.
//                        checkLocationCameraPermission();
                        mRequestingLocationUpdates = true;
                        binding.includeLoader.setVisibility(View.VISIBLE);
                        break;
                    case Activity.RESULT_CANCELED:
                        Log.i(TAG, "User chose not to make required location settings changes.");
                        mRequestingLocationUpdates = false;
                        binding.includeLoader.setVisibility(View.GONE);
                        break;
                }
                break;
        }
    }

    @Override
    public void onFailure(Throwable t, String api) {
        if (getActivity() != null) {
            t.getLocalizedMessage();
            binding.includeLoader.setVisibility(View.GONE);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (getContext() != null) {
            getContext().unregisterReceiver(gpsBroadcastReceiver);
        }
    }
}
