package com.grace.ui.customer;


import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.grace.R;
import com.grace.base.BaseActivity;
import com.grace.databinding.FragmentBookAppointmentBinding;
import com.grace.models.ExpertiseResponse;
import com.grace.models.GenericResponse;
import com.grace.models.ServiceProviderListResponse;
import com.grace.models.SubCategoriesResponse;
import com.grace.networks.APIHandler;
import com.grace.networks.APIResponseInterface;
import com.grace.ui.common.HolderActivity;
import com.grace.utils.CalendarPicker;
import com.grace.utils.Constants;
import com.grace.utils.PermissionsAndroid;
import com.grace.utils.Prefs;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class BookAppointment extends Fragment implements APIResponseInterface.OnCallableResponse, Constants.API_CALLS,
        Constants.PARAMS, View.OnClickListener, AdapterView.OnItemSelectedListener {

    private final APIHandler apiHandler;
    private FragmentBookAppointmentBinding binding;
    private List<SubCategoriesResponse.DataBean> servicesList;
    private String expertiseID = "", providerID = "";
    private int spinnerPosition = 0;
    private ServiceProviderListResponse.DataBean.SalonInfoBean salonInfoBean;
    private ExpertiseResponse.DataBean expertiseBean;
    private int  menQuantity, womenQuantity, kidsQuantity;
    private double menAmount, womenAmount, kidsAmount,totalAmount;
    private String latitude = "", longitude = "";
    private int PLACE_PICKER_REQUEST = 1;
    private Place place;
    private Bundle bundle;

    public BookAppointment() {
        apiHandler = new APIHandler(this);
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_book_appointment, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getActivity() != null && getArguments() != null) {
            bundle = getArguments();
            ((HolderActivity) getActivity()).setSupportActionBar(binding.toolbar);

            if (((HolderActivity) getActivity()).getSupportActionBar() != null) {
                ((HolderActivity) getActivity()).getSupportActionBar().setTitle("");
            }


            binding.toolbar.setNavigationIcon(R.drawable.ic_back);

            binding.toolbar.setNavigationOnClickListener(v -> {
                getActivity().onBackPressed();
            });

            expertiseBean = getArguments().getParcelable(EXPERTISE_LIST);
            if (expertiseBean != null)
                expertiseID = expertiseBean.getExpertiseId();
            providerID = getArguments().getString(PROVIDER_ID);
//            latitude = getArguments().getString(LATITUDE);
//            longitude = getArguments().getString(LONGITUDE);
            salonInfoBean = getArguments().getParcelable(SALON_INFO);

            if (expertiseBean != null)
                binding.toolbar.setTitle(expertiseBean.getName());

            binding.tvCheckOut.setOnClickListener(this);
            binding.etDateTime.setOnClickListener(this);
            binding.ivPlusMen.setOnClickListener(this);
            binding.ivMinusMen.setOnClickListener(this);
            binding.ivPlusWomen.setOnClickListener(this);
            binding.ivMinusWomen.setOnClickListener(this);
            binding.ivPlusKids.setOnClickListener(this);
            binding.ivMinusKids.setOnClickListener(this);
            binding.etServiceAddress.setOnClickListener(this);

            binding.spinnerServices.setOnItemSelectedListener(this);
            getSubCategories();

            setDate(getDate(), getTime());
        }
    }

    private String getTime() {
        SimpleDateFormat formatter = new SimpleDateFormat("hh:mm");

        Calendar calendar=Calendar.getInstance();
        calendar.add(Calendar.HOUR, 2);
        return formatter.format(calendar.getTimeInMillis());
    }

    private String getDate() {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        Calendar calendar=Calendar.getInstance();
//        calendar.add(Calendar.DATE, 1);
        return formatter.format(calendar.getTimeInMillis());
    }

    private void getSubCategories() {
        if (getActivity() != null) {
            if (((BaseActivity) getActivity()).isInternetAvailable(getActivity())) {
                binding.includeLoader.setVisibility(View.VISIBLE);
                Map<String, String> params = new HashMap<>();
                params.put(EXPERTISE_ID, expertiseID);
                apiHandler.getSubCategories(params, Constants.API_CALLS.SUBCATEGORIES_LIST);
            } else {
                Toast.makeText(getActivity(), R.string.error_internet, Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onSuccess(Response response, String api) {
        binding.includeLoader.setVisibility(View.GONE);
        if (getActivity() != null) {
            switch (api) {
                case BOOK_APPOINTMENT:
                    if (response.isSuccessful()) {
                        GenericResponse genericResponse = (GenericResponse) response.body();
                        if (genericResponse != null) {
                            Snackbar.make(binding.getRoot(), genericResponse.getMessage(), Snackbar.LENGTH_SHORT).show();
                            switch (genericResponse.getStatus()) {
                                case Constants.STATUS_200:
                                    getActivity().finish();
                                    break;
                            }
                        }
                    }

                    break;

                case SUBCATEGORIES_LIST:
                    if (response.isSuccessful()) {
                        servicesList = new ArrayList<>();
                        SubCategoriesResponse subCategoriesResponse = (SubCategoriesResponse) response.body();
                        if (subCategoriesResponse != null) {
                            servicesList = subCategoriesResponse.getData();
                            String[] subCategories = new String[servicesList.size() + 1];

                            subCategories[0] = "Select Services";

                            for (int i = 0; i < servicesList.size(); i++) {
                                SubCategoriesResponse.DataBean service = servicesList.get(i);
                                subCategories[i + 1] = service.getName();
                            }

                            ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_dropdown_item, subCategories);

                            binding.spinnerServices.setAdapter(arrayAdapter);

                        }
                    }


                    break;
            }
        }
    }

    @Override
    public void onFailure(Throwable t, String api) {
        if (getActivity() != null) {
            t.getLocalizedMessage();
            binding.includeLoader.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View v) {

        double amount = 0;

        if (getActivity() != null) {
            switch (v.getId()) {
                case R.id.tv_check_out:

                    if (spinnerPosition == 0) {
                        Snackbar.make(binding.getRoot(), "Please select service", Snackbar.LENGTH_SHORT).show();
                    } else if (TextUtils.isEmpty(binding.etDateTime.getText().toString().trim())) {
                        Snackbar.make(binding.getRoot(), "Please select date", Snackbar.LENGTH_SHORT).show();
                    } else if (TextUtils.isEmpty(binding.etServiceAddress.getText().toString().trim())) {
                        Snackbar.make(binding.getRoot(), "Please select address", Snackbar.LENGTH_SHORT).show();
                    } else if (totalAmount == 0) {
                        Snackbar.make(binding.getRoot(), "Please Select appointment for either men or women or kids", Snackbar.LENGTH_SHORT).show();
                    } else {
                        bundle.putString(DATE, binding.etDateTime.getText().toString());
                        bundle.putString(LATITUDE, latitude);
                        bundle.putString(LONGITUDE, longitude);
                        bundle.putString(TOTAL_AMOUNT, String.valueOf(totalAmount));
                        bundle.putString(MEN, String.valueOf(menQuantity));
                        bundle.putString(WOMEN, String.valueOf(womenQuantity));
                        bundle.putString(KIDS, String.valueOf(kidsQuantity));
                        bundle.putString(ADDRESS, binding.etServiceAddress.getText().toString());

                        ((HolderActivity) getActivity()).navigateToWithBundle(R.id.container, new ProviderListFragment(), null, true, bundle);

                    }
                    break;

                case R.id.et_date_time:
                    CalendarPicker.getInstance().showCalendar(getActivity(), this);
                    break;

                case R.id.et_service_address:
                    checkLocationCameraPermission();
                    break;
                case R.id.iv_plus_men:
                    if (spinnerPosition != 0) {
                        menQuantity++;
                        amount = Double.parseDouble(servicesList.get(spinnerPosition - 1).getAmount().getMen().replace("$", ""));
                        menAmount = amount * menQuantity;
                    } else {
                        Snackbar.make(binding.getRoot(), "Please select service first", Snackbar.LENGTH_SHORT).show();
                    }
                    break;
                case R.id.iv_minus_men:
                    if (spinnerPosition != 0) {
                        if (menQuantity > 0) {
                            menQuantity--;
                            amount = Double.parseDouble(servicesList.get(spinnerPosition - 1).getAmount().getMen().replace("$", ""));
                            menAmount = amount * menQuantity;
                        }
                    } else {
                        Snackbar.make(binding.getRoot(), "Please select service first", Snackbar.LENGTH_SHORT).show();
                    }
                    break;
                case R.id.iv_plus_women:
                    if (spinnerPosition != 0) {
                        womenQuantity++;
                        amount = Double.parseDouble(servicesList.get(spinnerPosition - 1).getAmount().getWomen().replace("$", ""));
                        womenAmount = amount * womenQuantity;
                    } else {
                        Snackbar.make(binding.getRoot(), "Please select service first", Snackbar.LENGTH_SHORT).show();
                    }
                    break;
                case R.id.iv_minus_women:
                    if (spinnerPosition != 0) {
                        if (womenQuantity > 0) {
                            womenQuantity--;
                            amount = Double.parseDouble(servicesList.get(spinnerPosition - 1).getAmount().getWomen().replace("$", ""));
                            womenAmount = amount * womenQuantity;
                        }
                    } else {
                        Snackbar.make(binding.getRoot(), "Please select service first", Snackbar.LENGTH_SHORT).show();
                    }
                    break;
                case R.id.iv_plus_kids:
                    if (spinnerPosition != 0) {
                        kidsQuantity++;
                        amount = Double.parseDouble(servicesList.get(spinnerPosition - 1).getAmount().getKids().replace("$", ""));
                        kidsAmount = amount * kidsQuantity;
                    } else {
                        Snackbar.make(binding.getRoot(), "Please select service first", Snackbar.LENGTH_SHORT).show();
                    }
                    break;
                case R.id.iv_minus_kids:
                    if (spinnerPosition != 0) {
                        if (kidsQuantity > 0) {
                            kidsQuantity--;
                            amount = Double.parseDouble(servicesList.get(spinnerPosition - 1).getAmount().getKids().replace("$", ""));
                            kidsAmount = amount * kidsQuantity;
                        }
                    } else {
                        Snackbar.make(binding.getRoot(), "Please select service first", Snackbar.LENGTH_SHORT).show();
                    }
                    break;
            }

            if (spinnerPosition != 0) {

                totalAmount = menAmount + womenAmount + kidsAmount;

                binding.tvMenQuantity.setText(String.valueOf(menQuantity));
                binding.tvWomenQuantity.setText(String.valueOf(womenQuantity));
                binding.tvKidsQuantity.setText(String.valueOf(kidsQuantity));


                binding.tvPayableAmount.setText(String.format("Total Payable\n$%s", totalAmount));

            }

        }
    }

    private void checkLocationCameraPermission() {
        if (getActivity() != null) {
            if (!PermissionsAndroid.getInstance().checkLocationPermission(getActivity())) {
                // Do not have permissions, request them now
                PermissionsAndroid.getInstance().requestForLocationPermission(this);
                return;
            }
            ((BaseActivity) getActivity()).showProgressDialog();
            try {
                PlacePicker.IntentBuilder placeBuilder = new PlacePicker.IntentBuilder();
                getActivity().startActivityForResult(placeBuilder.build(getActivity()), PLACE_PICKER_REQUEST);
            } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
                e.printStackTrace();
                ((BaseActivity) getActivity()).hideProgressDialog();
            }
        }
    }

    private void bookAppointment() {
        if (getActivity() != null) {
            if (((BaseActivity) getActivity()).isInternetAvailable(getActivity())) {
                if (spinnerPosition == 0) {
                    Snackbar.make(binding.getRoot(), "Please select service", Snackbar.LENGTH_SHORT).show();
                } else if (TextUtils.isEmpty(binding.etDateTime.getText().toString().trim())) {
                    Snackbar.make(binding.getRoot(), "Please select date", Snackbar.LENGTH_SHORT).show();
                } else if (TextUtils.isEmpty(binding.etServiceAddress.getText().toString().trim())) {
                    Snackbar.make(binding.getRoot(), "Please select address", Snackbar.LENGTH_SHORT).show();
                } else if (TextUtils.isEmpty(String.valueOf(totalAmount))) {
                    Snackbar.make(binding.getRoot(), "Please Select appointment for either men or women or kids", Snackbar.LENGTH_SHORT).show();
                } else {
                    binding.includeLoader.setVisibility(View.VISIBLE);
                    Map<String, String> params = new HashMap<>();
                    params.put(USER_ID, Prefs.getString(USER_ID, ""));
                    params.put(SERVICE_PROVIDER_ID, providerID);
                    params.put(SALON_ID, salonInfoBean.getSalonId());
                    params.put(TIME, binding.etDateTime.getText().toString().trim());
                    params.put(TOTAL_AMOUNT, String.valueOf(totalAmount));
                    params.put(MEN, String.valueOf(menQuantity));
                    params.put(WOMEN, String.valueOf(womenQuantity));
                    params.put(KIDS, String.valueOf(kidsQuantity));
                    params.put(LATITUDE, latitude);
                    params.put(ADDRESS, binding.etServiceAddress.getText().toString());
                    params.put(LONGITUDE, longitude);

                    params.put(EXPERTISE_ID, expertiseID);
                    apiHandler.bookAppointment(params, Constants.API_CALLS.BOOK_APPOINTMENT);
                }
            } else {
                Toast.makeText(getActivity(), R.string.error_internet, Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        spinnerPosition = position;
        switch (parent.getId()) {
            case R.id.spinner_services:
                if (position != 0) {
                    binding.tvMenPrice.setText(servicesList.get(position - 1).getAmount().getMen());
                    binding.tvWomenPrice.setText(servicesList.get(position - 1).getAmount().getWomen());
                    binding.tvKidsPrice.setText(servicesList.get(position - 1).getAmount().getKids());
                } else {
                    binding.tvPayableAmount.setText("Total Payable\n$0");
                    kidsQuantity = 0;
                    menQuantity = 0;
                    womenQuantity = 0;
                    womenAmount = 0;
                    menAmount = 0;
                    kidsAmount = 0;
                    totalAmount = 0;
                    binding.tvMenQuantity.setText("0");
                    binding.tvWomenQuantity.setText("0");
                    binding.tvKidsQuantity.setText("0");
                }
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


    public void setDate(String selectedDate, String selectedTime) {
        binding.etDateTime.setText(String.format("%s %s", selectedDate, selectedTime));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (getActivity() != null) {
            if (((BaseActivity) getActivity()).isShowing()) {
                ((BaseActivity) getActivity()).hideProgressDialog();
            }
            if (resultCode == RESULT_OK) {
                if (requestCode == PLACE_PICKER_REQUEST) {
                    place = PlacePicker.getPlace(getActivity(), data);
                    if (place != null) {
                        latitude = String.valueOf(place.getLatLng().latitude);
                        longitude = String.valueOf(place.getLatLng().longitude);
                        binding.etServiceAddress.setText(place.getAddress());

                    }
                }
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PermissionsAndroid.LOCATION_PERMISSION_REQUEST_CODE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getActivity(), "Location Permission Granted", Toast.LENGTH_SHORT).show();
                    checkLocationCameraPermission();
                } else if (Build.VERSION.SDK_INT >= 23 && permissions.length > 0 && !shouldShowRequestPermissionRationale(permissions[0])) {
                    System.out.println("Go to settings>>>>>>");

                } else {
                    Toast.makeText(getActivity(), "Location Permission denied", Toast.LENGTH_SHORT).show();
                }
            }

        }
    }
}
