package com.grace.ui.customer.adapters;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.grace.R;
import com.grace.databinding.ListItemExpertiseBinding;
import com.grace.interfaces.OnItemClickListener;
import com.grace.models.ExpertiseResponse;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ExpertiseAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final Context context;
    private int lastPosition = 0;
    private List<ExpertiseResponse.DataBean> list;
    private OnItemClickListener listener;

    public ExpertiseAdapter(Context context, List<ExpertiseResponse.DataBean> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ListItemExpertiseBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.list_item_expertise, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolder) {
            ((ViewHolder) holder).bindData(list.get(holder.getAdapterPosition()));
        }
        Animation animation = AnimationUtils.loadAnimation(context,
                (position > lastPosition) ? R.anim.up_from_bottom
                        : R.anim.down_from_top);
        holder.itemView.startAnimation(animation);
        lastPosition = position;
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void updateList(List<ExpertiseResponse.DataBean> list) {
        this.list=list;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ListItemExpertiseBinding binding;
        OnItemClickListener viewListener;

        ViewHolder(ListItemExpertiseBinding itemView) {
            super(itemView.getRoot());
            binding = itemView;

            binding.cvExpertise.setOnClickListener(this);
        }

        private void setOnItemClickListener(OnItemClickListener listener) {
            viewListener = listener;
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.cv_expertise:
                    viewListener.onItemClick(getAdapterPosition(), "");
                    break;
            }

        }

        void bindData(ExpertiseResponse.DataBean expertiseBean) {
            binding.tvExpertise.setText(expertiseBean.getName());
            String imageURL="";
            if (expertiseBean.getExpertiseId().equalsIgnoreCase("5bb6142ae03b86754d01b735")) {
                imageURL="http://graceapp.co.in/images/beauty-and-spa.jpg";
                binding.progress.setVisibility(View.VISIBLE);
                Picasso.get().load(imageURL).into(binding.ivExpertise, new Callback() {
                    @Override
                    public void onSuccess() {
                        binding.progress.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError(Exception e) {
                        e.printStackTrace();
                        binding.progress.setVisibility(View.GONE);
                    }

                });

            } else if (expertiseBean.getExpertiseId().equalsIgnoreCase("5bb61475e03b86757e502945")) {
                imageURL="http://graceapp.co.in/images/latest-trends.jpg";
                binding.progress.setVisibility(View.VISIBLE);
                Picasso.get().load(imageURL).into(binding.ivExpertise, new Callback() {
                    @Override
                    public void onSuccess() {
                        binding.progress.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError(Exception e) {
                        e.printStackTrace();
                        binding.progress.setVisibility(View.GONE);
                    }

                });

            }

            setOnItemClickListener(listener);
        }
    }

}
