package com.grace.ui.customer;


import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.grace.R;
import com.grace.base.BaseActivity;
import com.grace.databinding.FragmentPaymentBinding;
import com.grace.models.PaymentURLResponse;
import com.grace.networks.APIHandler;
import com.grace.networks.APIResponseInterface;
import com.grace.utils.BookingCustomDialog;
import com.grace.utils.Constants;
import com.grace.utils.PaymentResponseDialog;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class PaymentFragment extends Fragment implements Constants.PARAMS, Constants.API_CALLS,
        Constants, APIResponseInterface.OnCallableResponse {

    private final APIHandler apiHandler;
    FragmentPaymentBinding binding;
    private String bookingID = "", amount = "", serviceProviderID = "";
    private String url = "";

    public PaymentFragment() {
        apiHandler = new APIHandler(this);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_payment, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getActivity() != null && getArguments() != null) {
            amount = getArguments().getString(AMOUNT);
            bookingID = getArguments().getString(BOOKING_ID);
            serviceProviderID = getArguments().getString(SERVICE_PROVIDER_ID);

            ((BaseActivity) getActivity()).setSupportActionBar(binding.toolbar);
            if (((BaseActivity) getActivity()).getSupportActionBar() != null) {
                ((BaseActivity) getActivity()).getSupportActionBar().setTitle("");
            }

            binding.toolbar.setTitle(R.string.payment);

            binding.toolbar.setNavigationIcon(R.drawable.ic_back);

            binding.toolbar.setNavigationOnClickListener(v -> {
                getActivity().onBackPressed();
            });

//            getPaymentURL();

            /*https://us-central1-billedsampleproject.cloudfunctions.net/pay?uid=5bfcca4de03b867c27646062&amount=0.5*/


            System.out.println("AMOUNT--->" + amount);

            binding.includeLoader.setVisibility(View.VISIBLE);
            binding.wbPayment.getSettings().setJavaScriptEnabled(true);
            binding.wbPayment.loadUrl("https://us-central1-billedsampleproject.cloudfunctions.net/pay?uid=" + bookingID + "&amount=" + amount+"&providerID="+serviceProviderID);
            binding.wbPayment.setWebViewClient(new WebViewController());
        }
    }

    private void getPaymentURL() {
        if (getActivity() != null) {
            if (((BaseActivity) getActivity()).isInternetAvailable(getActivity())) {
                binding.includeLoader.setVisibility(View.VISIBLE);

//                amount=amount.substring(0,amount.indexOf("."));

                System.out.println("amount--->" + amount);

                Map<String, String> params = new HashMap<>();

                params.put(AMOUNT, amount);
                params.put(BOOKING_ID, bookingID);
                params.put(SERVICE_PROVIDER_ID, serviceProviderID);

                apiHandler.getPaymentURL(params, BOOKING_PAYMENT_PROCESS);
            } else {
                Toast.makeText(getActivity(), R.string.error_internet, Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onSuccess(Response response, String api) {
        binding.includeLoader.setVisibility(View.GONE);
        if (getActivity() != null) {
            switch (api) {
                case BOOKING_PAYMENT_PROCESS:
                    PaymentURLResponse paymentURLResponse = (PaymentURLResponse) response.body();
                    if (paymentURLResponse != null) {
                        switch (paymentURLResponse.getStatus()) {
                            case STATUS_200:
                                binding.includeLoader.setVisibility(View.VISIBLE);
                                binding.wbPayment.getSettings().setJavaScriptEnabled(true);
                                binding.wbPayment.loadUrl(paymentURLResponse.getRedirectUrl());
                                binding.wbPayment.setWebViewClient(new WebViewController());
                                break;
                        }
                    }
                    break;
            }
        }
    }

    @Override
    public void onFailure(Throwable t, String api) {
        if (getActivity() != null) {
            t.getLocalizedMessage();
            binding.includeLoader.setVisibility(View.GONE);
        }
    }

    public void paymentStatus(int status) {
        if (getActivity() != null) {
            switch (status) {
                case 0:
                    Intent intent = new Intent(API_CALLS.BOOKING_DETAILS);
                    intent.putExtra(PARAMS.PAYMENT_STATUS, 1);
                    getActivity().sendBroadcast(intent);
                case 1:
                case 2:
                    getActivity().onBackPressed();
                    break;
            }
        }
    }

    private class WebViewController extends WebViewClient {

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            System.out.println("WebViewController.shouldOverrideUrlLoading---->" + url);
            PaymentFragment.this.url = url;
            view.loadUrl(url);
//            binding.includeLoader.setVisibility(View.GONE);

            if (url.contains("http://graceapp.co.in/booking/payment/success.php")
                    || url.contains("https://graceapp.co.in/booking/payment/success.php")) {
//            if (url.contains("http://graceapp.co.in/booking/payment/successPaymentStatusChange.php")) {
                paymentResponseDialog(0);
            } else if (url.contains("http://graceapp.co.in/booking/payment/cancelPayment.php")
                    || url.contains("https://graceapp.co.in/booking/payment/cancelPayment.php")
                    || url.contains("cancel")) {
                paymentResponseDialog(1);
            } else if (url.contains("http://graceapp.co.in/booking/payment/paymentFailure.php")
                    || url.contains("https://graceapp.co.in/booking/payment/paymentFailure.php")) {
                paymentResponseDialog(2);
            }

            return true;
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            if (!binding.includeLoader.isActivated())
                binding.includeLoader.setVisibility(View.VISIBLE);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            binding.includeLoader.setVisibility(View.GONE);
        }
    }


    void paymentResponseDialog(int status) {
        if (getActivity() != null) {
            FragmentManager ft = getChildFragmentManager();
            PaymentResponseDialog paymentResponseDialog = new PaymentResponseDialog();
            paymentResponseDialog.setListener(this);
            paymentResponseDialog.setParams(status);
            paymentResponseDialog.show(ft, BookingCustomDialog.class.getSimpleName());
        }
    }
}
