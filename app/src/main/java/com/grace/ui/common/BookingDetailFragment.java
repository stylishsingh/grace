package com.grace.ui.common;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.grace.R;
import com.grace.base.BaseActivity;
import com.grace.databinding.FragmentBookingDetailBinding;
import com.grace.interfaces.SwipeListener;
import com.grace.models.BookingListResponse;
import com.grace.models.GenericResponse;
import com.grace.models.JobStatusResponse;
import com.grace.networks.APIHandler;
import com.grace.networks.APIResponseInterface;
import com.grace.utils.Constants;
import com.grace.utils.FeedbackDialog;
import com.grace.utils.Prefs;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Response;

import static com.grace.utils.Constants.API_CALLS.ACCEPT_BOOKING;
import static com.grace.utils.Constants.PARAMS.BOOKING_ID;
import static com.grace.utils.Constants.PARAMS.CUSTOMER_ID;
import static com.grace.utils.Constants.PARAMS.SERVICE_PROVIDER_ID;

/**
 * A simple {@link Fragment} subclass.
 */
public class BookingDetailFragment extends Fragment implements Constants, APIResponseInterface.OnCallableResponse, View.OnClickListener, SwipeListener {

    private final APIHandler apiHandler;
    FragmentBookingDetailBinding binding;
    BookingListResponse.DataBean dataBean;

    BroadcastReceiver receiverUpdateUI = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getExtras() != null) {
                if (intent.hasExtra(PARAMS.BOOKING_ID) && intent.getStringExtra(PARAMS.BOOKING_ID).equalsIgnoreCase(dataBean.getBookingId())) {
                    getBookingDetails(intent.getStringExtra(PARAMS.BOOKING_ID));
//                    MyFirebaseMessagingService.notificationManager.cancelAll();
                } else if (intent.hasExtra(PARAMS.PAYMENT_STATUS) && intent.getIntExtra(PARAMS.PAYMENT_STATUS, 0) == 1) {
                    binding.btnPay.setVisibility(View.GONE);
                    getBookingDetails(dataBean.getBookingId());
                }
            } else {
                getBookingDetails(dataBean.getBookingId());
            }
        }
    };

    public BookingDetailFragment() {
        apiHandler = new APIHandler(this);
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_booking_detail, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getActivity() != null && getArguments() != null) {

            getActivity().registerReceiver(receiverUpdateUI, new IntentFilter(API_CALLS.BOOKING_DETAILS));

            String bookingID = getArguments().getString(PARAMS.BOOKING_ID);

            ((BaseActivity) getActivity()).setSupportActionBar(binding.toolbar);

            if (((BaseActivity) getActivity()).getSupportActionBar() != null) {
                ((BaseActivity) getActivity()).getSupportActionBar().setTitle("");
            }

            binding.toolbar.setTitle(R.string.booking_detail);

            binding.toolbar.setNavigationIcon(R.drawable.ic_back);

            binding.toolbar.setNavigationOnClickListener(v -> {
                getActivity().onBackPressed();
            });

            binding.btnPay.setOnClickListener(this);


            binding.btnFeedback.setOnClickListener(this);
            binding.btnAccept.setOnClickListener(this);
            binding.swipeJob.setListener(this);

            getBookingDetails(bookingID);

        }
    }

    private void getBookingDetails(String bookingID) {
        if (getActivity() != null) {
            if (((BaseActivity) getActivity()).isInternetAvailable(getActivity())) {
                binding.includeLoader.setVisibility(View.VISIBLE);

                Map<String, String> params = new HashMap<>();
                params.put(BOOKING_ID, bookingID);

                apiHandler.getBookingDetails(params, API_CALLS.BOOKING_DETAILS);
            } else {
                Toast.makeText(getActivity(), R.string.error_internet, Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void setCustomerData() {
        if (getActivity() != null) {
            binding.cvDetailProvider.setVisibility(View.VISIBLE);
            binding.tvDateTime.setText(String.format(": %s", dataBean.getTime()));
            binding.tvAmount.setText(String.format(": $%s", dataBean.getTotalAmount()));

            if (dataBean.getAccept() == 1 && dataBean.getPaymentstatus() == 0) {
                binding.btnPay.setVisibility(View.VISIBLE);
                binding.tvBook.setText(R.string.booking);
            } else {
                if (dataBean.getPaymentstatus() == 1)
                    binding.tvBook.setText(R.string.booking_complete);
                else
                    binding.tvBook.setText(R.string.booking);
                binding.btnPay.setVisibility(View.GONE);
            }

            switch (dataBean.getJobStatus()) {
                case 0:
                    binding.viewBook.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.circle_primary_dark));
                    break;
                case 1:
                    binding.viewJobStarted.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.circle_primary_dark));
                    break;
                case 2:
                    if (dataBean.getPaymentstatus() == 1)
                        binding.btnFeedback.setVisibility(View.VISIBLE);
                    binding.viewJobFinished.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.circle_primary_dark));
                    break;
            }
        }
    }

    private void setProviderData() {

        binding.cvDetailCustomer.setVisibility(View.VISIBLE);
        binding.tvDateTimeCustomer.setText(String.format(": %s", dataBean.getTime()));
        binding.tvAmountCustomer.setText(String.format(": $%s", dataBean.getTotalAmount()));


        if (dataBean.getAccept() == 0) {
            binding.tvBookCustomer.setText(R.string.booking);
            binding.btnAccept.setVisibility(View.VISIBLE);
            binding.swipeJob.setVisibility(View.GONE);
        } else {
            binding.btnAccept.setVisibility(View.GONE);
            switch (dataBean.getJobStatus()) {
                case 0:
                    binding.swipeJob.jobStatus = "SWIPE TO START JOB";
                    binding.swipeJob.updateUI();
                    break;
                case 1:
                    binding.swipeJob.jobStatus = "SWIPE TO FINISH JOB";
                    binding.swipeJob.updateUI();
                    break;
            }
            if (dataBean.getPaymentstatus() == 1) {
                binding.tvBookCustomer.setText(R.string.booking_complete);
                switch (dataBean.getJobStatus()) {
                    case 0:
                        binding.swipeJob.setVisibility(View.VISIBLE);
                        binding.swipeJob.jobStatus = "SWIPE TO START JOB";
                        binding.swipeJob.updateUI();
                        break;
                    case 1:
                        binding.swipeJob.setVisibility(View.VISIBLE);
                        binding.swipeJob.jobStatus = "SWIPE TO FINISH JOB";
                        binding.swipeJob.updateUI();
                        break;
                    case 2:
                        binding.swipeJob.setVisibility(View.GONE);

                }
            } else {
                binding.tvBookCustomer.setText(R.string.booking);
            }

            if (!TextUtils.isEmpty(dataBean.getRating())) {
                binding.rlFeedback.setVisibility(View.VISIBLE);
                binding.tvFeedback.setText(dataBean.getComment());
                binding.rating.setRating(Float.parseFloat(dataBean.getRating()));
            }
        }


    }

    private void acceptBooking() {
        if (getActivity() != null) {
            if (((BaseActivity) getActivity()).isInternetAvailable(getActivity())) {
                binding.includeLoader.setVisibility(View.VISIBLE);
                Map<String, String> params = new HashMap<>();
                params.put(BOOKING_ID, dataBean.getBookingId());
                params.put(CUSTOMER_ID, dataBean.getUserInfo().getUserId());
                params.put(SERVICE_PROVIDER_ID, dataBean.getProviderInfo().getServiceProviderId());
                params.put(ACCEPT, "1");

                apiHandler.acceptBooking(params, ACCEPT_BOOKING);
            } else {
                Toast.makeText(getActivity(), R.string.error_internet, Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onSuccess(Response response, String api) {
        binding.includeLoader.setVisibility(View.GONE);
        switch (api) {
            case API_CALLS.JOB_STATUS_CHANGED:
                if (response.isSuccessful()) {
                    JobStatusResponse jobStatusResponse = (JobStatusResponse) response.body();
                    if (jobStatusResponse != null) {
                        Snackbar.make(binding.getRoot(), jobStatusResponse.getMessage(), Snackbar.LENGTH_SHORT).show();
                        switch (jobStatusResponse.getStatus()) {
                            case STATUS_200:
                                switch (jobStatusResponse.getData().getJobStatus()) {
                                    case 0:
                                        binding.swipeJob.jobStatus = "SWIPE TO START JOB";
                                        binding.swipeJob.updateUI();
                                        binding.swipeJob.collapseButton();
                                        break;
                                    case 1:
                                        binding.swipeJob.jobStatus = "SWIPE TO FINISH JOB";
                                        binding.swipeJob.updateUI();
                                        binding.swipeJob.collapseButton();
                                        break;
                                    case 2:
                                        binding.swipeJob.setVisibility(View.GONE);
                                        break;
                                }
                                dataBean.setJobStatus(jobStatusResponse.getData().getJobStatus());
                                break;
                            default:

                        }
                    }

                }
                break;
            case API_CALLS.BOOKING_DETAILS:
                if (response.isSuccessful()) {
                    BookingListResponse dataObj = (BookingListResponse) response.body();
                    if (dataObj != null) {
                        switch (dataObj.getStatus()) {
                            case STATUS_200:
                                dataBean = dataObj.getData().get(0);
                                if (Prefs.getString(PARAMS.ROLE, "").equalsIgnoreCase("1"))
                                    setCustomerData();
                                else
                                    setProviderData();
                                break;
                            case STATUS_0:

                                break;
                        }
                    }
                }


                break;
            case ACCEPT_BOOKING:
                if (response.isSuccessful()) {
                    GenericResponse genericResponse = (GenericResponse) response.body();
                    if (genericResponse != null) {
                        Snackbar.make(binding.getRoot(), genericResponse.getMessage(), Snackbar.LENGTH_SHORT).show();
                        switch (genericResponse.getStatus()) {
                            case STATUS_200:
                                getBookingDetails(dataBean.getBookingId());
                                break;
                            case STATUS_0:

                                break;
                        }
                    }
                }
                break;
        }
    }

    @Override
    public void onFailure(Throwable t, String api) {
        if (getActivity() != null) {
            t.getLocalizedMessage();
            binding.includeLoader.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_feedback:
                bookingSuccessDialog();
                break;
            case R.id.btn_accept:
                acceptBooking();
                break;
            case R.id.btn_pay:
                startActivity(new Intent(getActivity(), HolderActivity.class)
                        .putExtra(NAVIGATE_SCREENS.SCREEN, NAVIGATE_SCREENS.PAYMENT_SCREEN)
                        .putExtra(BOOKING_ID, dataBean.getBookingId())
                        .putExtra(SERVICE_PROVIDER_ID, dataBean.getProviderInfo().getServiceProviderId())
                        .putExtra(PARAMS.AMOUNT, dataBean.getTotalAmount()));
                break;
        }
    }

    private void bookingSuccessDialog() {
        if (getActivity() != null) {
            FragmentManager ft = getChildFragmentManager();
            FeedbackDialog feedbackDialog = new FeedbackDialog();
            feedbackDialog.setListener(this, dataBean.getBookingId(), dataBean.getProviderInfo().getSalonId(),
                    dataBean.getProviderInfo().getServiceProviderId(), dataBean.getUserInfo().getUserId());
            feedbackDialog.show(ft, FeedbackDialog.class.getSimpleName());
        }
    }

    @Override
    public void selectData() {
        updateJobStatus();
    }

    void updateJobStatus() {
        if (getActivity() != null) {
            if (((BaseActivity) getActivity()).isInternetAvailable(getActivity())) {
                binding.includeLoader.setVisibility(View.VISIBLE);

                Map<String, String> params = new HashMap<>();
                params.put(PARAMS.USER_ID, Prefs.getString(PARAMS.USER_ID, ""));
                params.put(PARAMS.SALON_ID, dataBean.getProviderInfo().getSalonId());
                params.put(PARAMS.SERVICE_PROVIDER_ID, dataBean.getProviderInfo().getServiceProviderId());
                params.put(BOOKING_ID, dataBean.getBookingId());
                params.put(CUSTOMER_ID, dataBean.getUserInfo().getUserId());
                params.put(PARAMS.JOB_STATUS, String.valueOf(dataBean.getJobStatus() + 1));

                apiHandler.jobStatusChanged(params, API_CALLS.JOB_STATUS_CHANGED);
            } else {
                Toast.makeText(getActivity(), R.string.error_internet, Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (getActivity() != null) {
            getActivity().unregisterReceiver(receiverUpdateUI);
        }
    }
}
