package com.grace.ui.common;

import android.content.Intent;
import android.os.Bundle;

import com.grace.R;
import com.grace.base.BaseActivity;
import com.grace.ui.customer.BookAppointment;
import com.grace.ui.customer.PaymentFragment;
import com.grace.ui.customer.ProviderListFragment;
import com.grace.utils.Constants;

public class HolderActivity extends BaseActivity implements Constants.NAVIGATE_SCREENS {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_holder);

        if (getIntent()!=null){
            loadFragment(getIntent());
        }

    }

    private void loadFragment(Intent intent){
        String navigateTo=intent.getStringExtra(SCREEN);
        switch (navigateTo){
            case PROVIDER_SCREEN:
                navigateToWithBundle(R.id.container,new ProviderListFragment(),
                        ProviderListFragment.class.getSimpleName(),true,getIntent().getExtras());
            break;
            case BOOK_APPOINTMENT:
                navigateToWithBundle(R.id.container,new BookAppointment(),
                        BookAppointment.class.getSimpleName(),true,getIntent().getExtras());
            break;

            case VIEW_IMAGE_SCREEN:
                navigateToWithBundle(R.id.container,new ViewImagesFragment(),
                        ViewImagesFragment.class.getSimpleName(),false,getIntent().getExtras());
            break;

            case PAYMENT_SCREEN:
                navigateToWithBundle(R.id.container,new PaymentFragment(),
                        PaymentFragment.class.getSimpleName(),false,getIntent().getExtras());
            break;

            case BOOKING_DETAIL_SCREEN:
                navigateToWithBundle(R.id.container,new BookingDetailFragment(),
                        BookingDetailFragment.class.getSimpleName(),false,getIntent().getExtras());
            break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        ((getSupportFragmentManager())).findFragmentById(R.id.container).onActivityResult(requestCode, resultCode, data);
    }
}
