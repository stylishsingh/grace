package com.grace.ui.common;


import android.content.DialogInterface;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.grace.R;
import com.grace.databinding.FragmentStartupBinding;

import static com.grace.utils.Constants.PARAMS.ROLE;

/**
 * A simple {@link Fragment} subclass.
 */
public class StartupFragment extends Fragment implements View.OnClickListener {

    FragmentStartupBinding binding;

    public StartupFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_startup, container, false);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        binding.btnSignIn.setOnClickListener(this);
        binding.btnCreateAccount.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if (getActivity()!=null) {
            switch (v.getId()) {
                case R.id.btn_sign_in:
                    ((LoginSignUpActivity)getActivity()).navigateToWithBundle(R.id.container, new LoginFragment(), LoginFragment.class.getSimpleName(), false, null);
                    break;
                case R.id.btn_create_account:
                    showChooseDialog();
                    break;
            }
        }
    }

    private void showChooseDialog() {
        if (getActivity() != null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            String[] options = {"Customer", "Service Provider", "Cancel"};
            final Bundle bundle = new Bundle();
            builder.setTitle("Please Choose User Role")
                    .setItems(options, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // The 'which' argument contains the index position
                            switch (which) {
                                case 0:
                                    bundle.putString(ROLE, "1");
                                    ((LoginSignUpActivity) getActivity())
                                            .navigateToWithBundle(R.id.container, new SignUpFragment(), SignUpFragment.class.getSimpleName(), false, bundle);
                                    break;
                                case 1:
                                    bundle.putString(ROLE, "2");
                                    ((LoginSignUpActivity) getActivity())
                                            .navigateToWithBundle(R.id.container, new SignUpFragment(), SignUpFragment.class.getSimpleName(), false, bundle);
                                    break;
                                case 2:
                                    dialog.dismiss();
                                    break;
                            }
                        }
                    });
            builder.setCancelable(false);
            builder.show();
        }
    }

}
