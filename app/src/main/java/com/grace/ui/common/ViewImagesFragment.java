package com.grace.ui.common;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.grace.R;
import com.grace.databinding.FragmentViewImageBinding;
import com.grace.utils.Constants;
import com.grace.utils.TouchImageView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.io.File;


public class ViewImagesFragment extends Fragment implements Constants.PARAMS {
    private View rootView;
    private String profilePicUrl = "", imageTitle = "";
    private FragmentViewImageBinding binding;

    //request code
    private int IMAGE_CAPTURE_REQUEST_CODE = 0;
    private int IMAGE_CHOOSER_REQUEST_CODE = 1;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_view_image, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews();
    }

    private void initViews() {
        try {
            if (getArguments() != null && getArguments().getString(BROWSE_PIC) != null) {
                profilePicUrl = getArguments().getString(BROWSE_PIC);
                imageTitle = getArguments().getString(PIC_TITLE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (getActivity() instanceof HolderActivity) {
            if (getActivity() != null) {
                ((HolderActivity) getActivity()).setSupportActionBar(binding.toolbar);
                if (((HolderActivity) getActivity()).getSupportActionBar() != null)
                    ((HolderActivity) getActivity()).getSupportActionBar().setTitle("");
                binding.toolbar.setTitle(imageTitle);
                binding.toolbar.setNavigationIcon(R.drawable.ic_back);
                binding.toolbar.setNavigationOnClickListener(v -> getActivity().onBackPressed());
            }

        }


        if (!TextUtils.isEmpty(profilePicUrl)) {
            binding.progressBar.setVisibility(View.VISIBLE);

            if (profilePicUrl.startsWith("http")) {
                Picasso.get().load(profilePicUrl).resize(500, 500).memoryPolicy(MemoryPolicy.NO_CACHE).into(binding.profilePic, new Callback() {
                    @Override
                    public void onSuccess() {
                        binding.progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError(Exception e) {
                        binding.progressBar.setVisibility(View.GONE);
                    }
                });
            } else {
                Picasso.get().load(new File(profilePicUrl)).resize(500, 500).memoryPolicy(MemoryPolicy.NO_CACHE).into(binding.profilePic, new Callback() {
                    @Override
                    public void onSuccess() {
                        binding.progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError(Exception e) {
                        binding.progressBar.setVisibility(View.GONE);
                    }
                });
            }

        }
    }

}
