package com.grace.ui.common;

import android.os.Bundle;

import com.grace.R;
import com.grace.base.BaseActivity;

public class LoginSignUpActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_sign_up);

        navigateToWithBundle(R.id.container, new StartupFragment(), StartupFragment.class.getSimpleName(), false, null);

    }
}
