package com.grace.ui.common;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.grace.R;
import com.grace.base.BaseActivity;
import com.grace.models.SignUpResponse;
import com.grace.models.UserDataResponse;
import com.grace.networks.APIHandler;
import com.grace.networks.APIResponseInterface;
import com.grace.utils.Constants;
import com.grace.utils.Prefs;
import com.grace.utils.Utils;
import com.grace.utils.Validations;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class SignUpFragment extends Fragment implements View.OnClickListener,
        APIResponseInterface.OnCallableResponse, Constants.API_CALLS, Constants.PARAMS {

    private final APIHandler apiHandler;
    private Button btnSignUp;
    private TextView tvLogin;
    private TextInputEditText etEmail, etPhone, etPassword, etConfirmPassword;
    private String role;

    public SignUpFragment() {
        // Required empty public constructor
        apiHandler = new APIHandler(this);
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_sign_up, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getArguments() != null) {
            role = getArguments().getString(ROLE);
        }

        /*assigning references to the views*/
        btnSignUp = view.findViewById(R.id.btn_sign_up);
        tvLogin = view.findViewById(R.id.tv_login);
        etEmail = view.findViewById(R.id.et_email);
        etPhone = view.findViewById(R.id.et_phone);
        etPassword = view.findViewById(R.id.et_password);
        etConfirmPassword = view.findViewById(R.id.et_confirm_password);

        /*set listener on views*/
        btnSignUp.setOnClickListener(this);
        tvLogin.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (getActivity() != null) {
            switch (v.getId()) {
                case R.id.btn_sign_up:
                    if (Utils.getInstance().isInternetAvailable(getActivity())) {
                        String email = etEmail.getText().toString().trim();
                        String phone = etPhone.getText().toString().trim();
                        String password = etPassword.getText().toString().trim();
                        String confirmPassword = etConfirmPassword.getText().toString().trim();
                        String response = Validations.getInstance().validateSignUpFields(email, phone,
                                role, password, confirmPassword);
                        if (response.isEmpty()) {
                            ((BaseActivity) getActivity()).showProgressDialog();
                            Map<String, String> params = new HashMap<>();
                            params.put(EMAIL, email);
                            params.put(PHONE, phone);
                            params.put(ROLE, role);
                            params.put(PASSWORD, password);
                            apiHandler.signUp(params, SIGN_UP);
                        } else {
                            Utils.getInstance().showSnackBar(getView(), response);
                        }
                    } else {
                        Utils.getInstance().showSnackBar(getView(), getString(R.string.error_internet));
                    }

                    break;
                case R.id.tv_login:
                    ((LoginSignUpActivity) getActivity())
                            .navigateToWithBundle(R.id.container, new LoginFragment(), LoginFragment.class.getSimpleName(), false, null);
                    break;
            }
        }
    }

    @Override
    public void onSuccess(Response response, String api) {
        if (getActivity() != null) {
            if (((BaseActivity) getActivity()).isShowing())
                ((BaseActivity) getActivity()).hideProgressDialog();
            switch (api) {
                case SIGN_UP:
                    if (response.isSuccessful()) {
                        SignUpResponse signUpResponse = (SignUpResponse) response.body();
                        if (signUpResponse != null) {
                            Snackbar.make(btnSignUp, signUpResponse.getMessage(), Snackbar.LENGTH_SHORT).show();
                            switch (signUpResponse.getStatus()) {
                                case Constants.STATUS_200:
                                    UserDataResponse dataObj = signUpResponse.getData().get(0);
                                    Toast.makeText(getActivity(), signUpResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                    Prefs.putString(EMAIL, dataObj.getEmail());
                                    Prefs.putString(PHONE, dataObj.getPhone());
                                    if (role.equals("2")) {
                                        startActivity(new Intent(getActivity(), ProfileActivity.class)
                                                .putExtra(getString(R.string.isCustomer), false)
                                                .putExtra(getString(R.string.isDashboard), false)
                                                .putExtra(USER_ID, dataObj.getUserId())
                                                .putExtra(ROLE, role));
                                    } else {
                                        startActivity(new Intent(getActivity(), ProfileActivity.class)
                                                .putExtra(getString(R.string.isCustomer), true)
                                                .putExtra(getString(R.string.isDashboard), false)
                                                .putExtra(USER_ID, dataObj.getUserId())
                                                .putExtra(ROLE, role));

                                /*Prefs.putString(USER_ID, dataObj.getUserId());
                                Prefs.putString(ROLE, dataObj.getRole());
                                Prefs.putString(EXPERTISE_ID, dataObj.getExpertise());
                                Prefs.putString(SALON_NAME, dataObj.getSalonName());
                                Prefs.putString(LATITUDE, String.valueOf(dataObj.getLatitude()));
                                Prefs.putString(LONGITUDE, String.valueOf(dataObj.getLongitude()));
                                Prefs.putString(SALON_IMAGE, dataObj.getSalonImage());
                                Prefs.putString(FULL_NAME, dataObj.getFullName());
                                Prefs.putString(ADDRESS, dataObj.getAddress());
                                Prefs.putBoolean(IS_LOGIN, true);*/

//                                startActivity(new Intent(getActivity(), CustomerDashboard.class));

                                    }
                                    getActivity().finish();
                                    break;
                            }


                        }
                    }
                    break;
            }
        }
    }

    @Override
    public void onFailure(Throwable t, String api) {
        if (getActivity() != null) {
            Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            if (((BaseActivity) getActivity()).isShowing())
                ((BaseActivity) getActivity()).hideProgressDialog();
        }
    }
}
