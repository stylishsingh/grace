package com.grace.ui.common.adapters;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.grace.R;
import com.grace.databinding.ListItemBookingsBinding;
import com.grace.interfaces.OnItemClickListener;
import com.grace.models.BookingListResponse;
import com.grace.utils.Constants;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

public class BookingAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final Context context;
    private int lastPosition = 0;
    private List<BookingListResponse.DataBean> list;
    private OnItemClickListener listener;
    private boolean isCustomer;

    public BookingAdapter(Context context, List<BookingListResponse.DataBean> list, boolean isCustomer) {
        this.context = context;
        this.list = list;
        this.isCustomer = isCustomer;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ListItemBookingsBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.list_item_bookings, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolder) {
            ((ViewHolder) holder).bindData(list.get(holder.getAdapterPosition()));
        }
        Animation animation = AnimationUtils.loadAnimation(context,
                (position > lastPosition) ? R.anim.up_from_bottom
                        : R.anim.down_from_top);
        holder.itemView.startAnimation(animation);
        lastPosition = position;
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void updateList(List<BookingListResponse.DataBean> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ListItemBookingsBinding binding;
        OnItemClickListener viewListener;

        ViewHolder(ListItemBookingsBinding itemView) {
            super(itemView.getRoot());
            binding = itemView;

            binding.cvBooking.setOnClickListener(this);
            binding.ivAccept.setOnClickListener(this);
        }

        private void setOnItemClickListener(OnItemClickListener listener) {
            viewListener = listener;
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.cv_booking:
                    viewListener.onItemClick(getAdapterPosition(), Constants.DETAIL);
                    break;
                case R.id.iv_accept:
                    viewListener.onItemClick(getAdapterPosition(), Constants.ACCEPT);
                    break;
            }

        }

        void bindData(BookingListResponse.DataBean dataBean) {
            if (!isCustomer) {

                /*switch (dataBean.getAccept()) {
                    case 0:
                        binding.ivAccept.setVisibility(View.VISIBLE);
                        break;
                    case 1:
                        binding.ivAccept.setVisibility(View.INVISIBLE);
                        break;
                }*/


                if (!TextUtils.isEmpty(dataBean.getUserInfo().getImage()))
                    Picasso.get().load(dataBean.getUserInfo().getImage()).resize(300, 300).resize(80, 80).into(binding.ivPic, new Callback() {
                        @Override
                        public void onSuccess() {
                            binding.progress.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError(Exception e) {
                            binding.progress.setVisibility(View.GONE);
                        }

                    });
                else
                    binding.ivPic.setImageResource(R.drawable.ic_profile);

                binding.tvName.setText(dataBean.getUserInfo().getName());
                binding.tvPhone.setText(String.format("Phone: %s", dataBean.getUserInfo().getPhone()));
                binding.tvTime.setText(String.format("Date: %s", dataBean.getTime()));
            } else {
                if (!TextUtils.isEmpty(dataBean.getProviderInfo().getImage()))
                    Picasso.get().load(dataBean.getProviderInfo().getImage()).resize(300, 300).resize(80, 80).into(binding.ivPic, new Callback() {
                        @Override
                        public void onSuccess() {
                            binding.progress.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError(Exception e) {
                            binding.progress.setVisibility(View.GONE);
                        }

                    });
                else
                    binding.ivPic.setImageResource(R.drawable.ic_profile);

                binding.tvName.setText(dataBean.getProviderInfo().getName());
                binding.tvPhone.setText(String.format("Phone: %s", dataBean.getProviderInfo().getPhone()));
                binding.tvTime.setText(String.format("Date: %s", dataBean.getTime()));
            }
            setOnItemClickListener(listener);
        }
    }

}
