package com.grace.ui.common;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.grace.R;
import com.grace.base.BaseActivity;
import com.grace.ui.customer.CustomerProfileFragment;
import com.grace.ui.provider.ProviderProfileFragment;
import com.grace.utils.Constants;

public class ProfileActivity extends BaseActivity implements Constants.PARAMS {

    public Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle("");
        toolbar.setTitle(R.string.header_profile);
        if (getIntent() != null) {
            if (getIntent().getBooleanExtra(getString(R.string.isCustomer), false))
                navigateToWithBundle(R.id.container, new CustomerProfileFragment(), CustomerProfileFragment.class.getSimpleName(), false, getIntent().getExtras());
            else
                navigateToWithBundle(R.id.container, new ProviderProfileFragment(), ProviderProfileFragment.class.getSimpleName(), false, getIntent().getExtras());
        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        (getSupportFragmentManager().findFragmentById(R.id.container)).
                onActivityResult(requestCode, resultCode, data);
    }
}
