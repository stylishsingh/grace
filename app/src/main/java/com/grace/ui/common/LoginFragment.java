package com.grace.ui.common;


import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.grace.R;
import com.grace.base.BaseActivity;
import com.grace.models.LoginResponse;
import com.grace.models.UserDataResponse;
import com.grace.networks.APIHandler;
import com.grace.networks.APIResponseInterface;
import com.grace.ui.customer.CustomerDashboard;
import com.grace.ui.provider.ProviderDashboard;
import com.grace.utils.Constants;
import com.grace.utils.Prefs;
import com.grace.utils.Utils;
import com.grace.utils.Validations;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends Fragment implements View.OnClickListener,
        APIResponseInterface.OnCallableResponse, Constants.API_CALLS, Constants.PARAMS {

    private final APIHandler apiHandler;
    private Button btnSignIn;
    private TextView tvSignUp, tvForgotPassword;
    private TextInputEditText etEmail, etPassword;

    public LoginFragment() {
        apiHandler = new APIHandler(this);
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_login, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        /*assigning references to the views*/
        btnSignIn = view.findViewById(R.id.btn_sign_in);
        tvSignUp = view.findViewById(R.id.tv_sign_up);
        tvForgotPassword = view.findViewById(R.id.tv_forgot_password);
        etEmail = view.findViewById(R.id.et_email);
        etPassword = view.findViewById(R.id.et_password);

        /*set listener on views*/
        btnSignIn.setOnClickListener(this);
        tvForgotPassword.setOnClickListener(this);
        tvSignUp.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if (getActivity() != null) {
            switch (v.getId()) {
                case R.id.btn_sign_in:

                    if (Utils.getInstance().isInternetAvailable(getActivity())) {
                        String email = etEmail.getText().toString().trim();
                        String password = etPassword.getText().toString().trim();
                        String response = Validations.getInstance().validateLoginFields(email, password);
                        if (response.isEmpty()) {
                            ((BaseActivity) getActivity()).showProgressDialog();
                            Map<String, String> params = new HashMap<>();
                            params.put(EMAIL, email);
                            params.put(PASSWORD, password);
                            apiHandler.login(params, LOGIN);
                        } else {

                            Utils.getInstance().showSnackBar(getView(), response);
                        }
                    } else {
                        Utils.getInstance().showSnackBar(getView(), getString(R.string.error_internet));
                    }

                    break;
                case R.id.tv_sign_up:
                    showChooseDialog();
                    break;
                case R.id.tv_forgot_password:
                    ((LoginSignUpActivity) getActivity())
                            .navigateToWithBundle(R.id.container, new ForgotPassword(), ForgotPassword.class.getSimpleName(), false, null);
                    break;
            }
        }
    }

    @Override
    public void onSuccess(Response response, String api) {
        if (getActivity() != null) {
            if (((BaseActivity) getActivity()).isShowing())
                ((BaseActivity) getActivity()).hideProgressDialog();
            switch (api) {
                case LOGIN:
                    if (response.isSuccessful()) {
                        LoginResponse loginResponse = (LoginResponse) response.body();
                        if (loginResponse != null) {

                            Toast.makeText(getActivity(), loginResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            if (loginResponse.getStatus() == 200) {
                                UserDataResponse dataObj = loginResponse.getData().get(0);
                                Prefs.putString(USER_ID, dataObj.getUserId());
                                Prefs.putString(EMAIL, dataObj.getEmail());
                                Prefs.putString(ROLE, dataObj.getRole());
                                Prefs.putString(PHONE, dataObj.getPhone());
                                Prefs.putString(EXPERTISE_ID, dataObj.getExpertise());
                                Prefs.putString(SALON_NAME, dataObj.getSalonName());
                                Prefs.putString(LATITUDE, String.valueOf(dataObj.getLatitude()));
                                Prefs.putString(LONGITUDE, String.valueOf(dataObj.getLongitude()));
                                Prefs.putString(SALON_IMAGE, dataObj.getSalonImage());
                                Prefs.putString(FULL_NAME, dataObj.getFullName());
                                Prefs.putString(ADDRESS, dataObj.getAddress());

                                if (dataObj.getRole().equals("1")) {
                                    if (dataObj.getLatitude() == 0 || dataObj.getLongitude() == 0 || TextUtils.isEmpty(dataObj.getAddress())
                                            || TextUtils.isEmpty(dataObj.getFullName())) {

                                        getActivity().startActivity(new Intent(getActivity(), ProfileActivity.class)
                                                .putExtra(USER_ID, dataObj.getUserId())
                                                .putExtra(ROLE, dataObj.getRole())
                                                .putExtra(getString(R.string.isDashboard), false)
                                                .putExtra(getString(R.string.isCustomer), true));
                                    } else {
                                        Prefs.putBoolean(IS_LOGIN, true);
                                        getActivity().startActivity(new Intent(getActivity(), CustomerDashboard.class));
                                    }

                                } else {
                                    if (dataObj.getLatitude() == 0 || dataObj.getLongitude() == 0
                                            || dataObj.getExpertise().isEmpty() || TextUtils.isEmpty(dataObj.getFullName()) || TextUtils.isEmpty(dataObj.getAddress())) {
                                        getActivity().startActivity(new Intent(getActivity(), ProfileActivity.class)
                                                .putExtra(USER_ID, dataObj.getUserId())
                                                .putExtra(ROLE, dataObj.getRole())
                                                .putExtra(getString(R.string.isDashboard), false)
                                                .putExtra(getString(R.string.isCustomer), false));
                                    } else {
                                        Prefs.putBoolean(IS_LOGIN, true);
                                        getActivity().startActivity(new Intent(getActivity(), ProviderDashboard.class));
                                    }
                                }
                                getActivity().finish();
                            }
                        }
                    }
                    break;
            }
        }
    }

    @Override
    public void onFailure(Throwable t, String api) {
        if (getActivity() != null) {
            Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            if (((BaseActivity) getActivity()).isShowing())
                ((BaseActivity) getActivity()).hideProgressDialog();
        }
    }


    private void showChooseDialog() {
        if (getActivity() != null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            String[] options = {"Customer", "Service Provider", "Cancel"};
            final Bundle bundle = new Bundle();
            builder.setTitle("Please Choose User Role")
                    .setItems(options, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // The 'which' argument contains the index position
                            switch (which) {
                                case 0:
                                    bundle.putString(ROLE, "1");
                                    ((LoginSignUpActivity) getActivity())
                                            .navigateToWithBundle(R.id.container, new SignUpFragment(), SignUpFragment.class.getSimpleName(), false, bundle);
                                    break;
                                case 1:
                                    bundle.putString(ROLE, "2");
                                    ((LoginSignUpActivity) getActivity())
                                            .navigateToWithBundle(R.id.container, new SignUpFragment(), SignUpFragment.class.getSimpleName(), false, bundle);
                                    break;
                                case 2:
                                    dialog.dismiss();
                                    break;
                            }
                        }
                    });
            builder.setCancelable(false);
            builder.show();
        }
    }
}
