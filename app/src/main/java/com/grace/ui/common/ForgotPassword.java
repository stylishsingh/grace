package com.grace.ui.common;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.transition.Explode;
import android.support.transition.TransitionManager;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.grace.R;
import com.grace.base.BaseActivity;
import com.grace.databinding.FragmentForgotPasswordBinding;
import com.grace.models.ForgotPasswordResponse;
import com.grace.models.GenericResponse;
import com.grace.networks.APIHandler;
import com.grace.networks.APIResponseInterface;
import com.grace.utils.Constants;
import com.grace.utils.Utils;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ForgotPassword extends Fragment implements APIResponseInterface.OnCallableResponse, Constants, View.OnClickListener {


    private final APIHandler apiHandler;
    FragmentForgotPasswordBinding binding;
    private String userID;

    public ForgotPassword() {
        apiHandler = new APIHandler(this);
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_forgot_password, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.btnVerify.setOnClickListener(this);
        binding.btnReset.setOnClickListener(this);
        binding.tvBack.setOnClickListener(this);
    }

    @Override
    public void onSuccess(Response response, String api) {
        if (getActivity() != null) {
            binding.includeLoader.setVisibility(View.GONE);
            switch (api) {
                case API_CALLS.FORGOT_PASSWORD:
                    if (response.isSuccessful()) {
                        ForgotPasswordResponse forgotPasswordResponse = (ForgotPasswordResponse) response.body();
                        if (forgotPasswordResponse != null) {
                            switch (forgotPasswordResponse.getStatus()) {
                                case STATUS_200:
                                    userID = forgotPasswordResponse.getData().getUserId();
                                    if (Utils.getInstance().isEqualLollipop()) {
                                        TransitionManager.beginDelayedTransition((ViewGroup) binding.getRoot(), new Explode());
                                    }
                                    binding.tilEmail.setVisibility(View.GONE);
                                    binding.btnVerify.setVisibility(View.GONE);
                                    binding.tvBack.setVisibility(View.GONE);
                                    binding.tilPassword.setVisibility(View.VISIBLE);
                                    binding.tilOtp.setVisibility(View.VISIBLE);
                                    binding.tilConfirmPassword.setVisibility(View.VISIBLE);
                                    binding.btnReset.setVisibility(View.VISIBLE);

                                    break;
                                default:
                                    userID = "";
                            }
                        }
                    }

                    break;

                case API_CALLS.RESET_PASSWORD:
                    if (response.isSuccessful()) {
                        GenericResponse genericResponse = (GenericResponse) response.body();
                        if (genericResponse != null) {
                            Toast.makeText(getActivity(), genericResponse.getMessage(), Toast.LENGTH_SHORT).show();

                            ((LoginSignUpActivity) getActivity())
                                    .navigateToWithBundle(R.id.container, new LoginFragment(), LoginFragment.class.getSimpleName(), false, null);
                        }
                    }
                    break;
            }
        }
    }


    @Override
    public void onFailure(Throwable t, String api) {
        if (getActivity() != null) {
            t.getLocalizedMessage();
            binding.includeLoader.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View v) {
        if (getActivity()!=null) {
            switch (v.getId()) {
                case R.id.btn_verify:
                    verifyEmail();
                    break;
                case R.id.btn_reset:
                    resetPassword();
                    break;

                case R.id.tv_back:
                    ((LoginSignUpActivity) getActivity())
                            .navigateToWithBundle(R.id.container, new LoginFragment(), LoginFragment.class.getSimpleName(), false, null);
                    break;
            }
        }
    }

    private void verifyEmail() {
        if (getActivity() != null) {
            if (((BaseActivity) getActivity()).isInternetAvailable(getActivity())) {
                String email = binding.etEmail.getText().toString().trim();
                if (TextUtils.isEmpty(email)) {
                    Snackbar.make(binding.getRoot(), R.string.error_email, Snackbar.LENGTH_SHORT).show();
                } else {
                    binding.includeLoader.setVisibility(View.VISIBLE);
                    Map<String, String> params = new HashMap<>();
                    params.put(PARAMS.EMAIL, email);
                    apiHandler.forgotPassword(params, API_CALLS.FORGOT_PASSWORD);
                }
            } else {
                Toast.makeText(getActivity(), R.string.error_internet, Toast.LENGTH_SHORT).show();
            }
        }
    }


    private void resetPassword() {
        if (getActivity() != null) {
            if (((BaseActivity) getActivity()).isInternetAvailable(getActivity())) {
                String password = binding.etPassword.getText().toString().trim();
                String confirmPassword = binding.etConfirmPassword.getText().toString().trim();
                String otp = binding.etOtp.getText().toString().trim();
                if (TextUtils.isEmpty(password)) {
                    Snackbar.make(binding.getRoot(), "Please enter otp.", Snackbar.LENGTH_SHORT).show();
                }else if (TextUtils.isEmpty(password)) {
                    Snackbar.make(binding.getRoot(), "Please enter password.", Snackbar.LENGTH_SHORT).show();
                } else if (TextUtils.isEmpty(confirmPassword)) {
                    Snackbar.make(binding.getRoot(), "Please enter confirm password.", Snackbar.LENGTH_SHORT).show();
                } else if (!password.equals(confirmPassword)) {
                    Snackbar.make(binding.getRoot(), "Password and confirm password don't match.", Snackbar.LENGTH_SHORT).show();
                } else {
                    binding.includeLoader.setVisibility(View.VISIBLE);
                    Map<String, String> params = new HashMap<>();
                    params.put(PARAMS.USER_ID, userID);
                    params.put(PARAMS.OTP, otp);
                    params.put(PARAMS.NEW_PASSWORD, password);
                    apiHandler.resetPassword(params, API_CALLS.RESET_PASSWORD);
                }
            } else {
                Toast.makeText(getActivity(), R.string.error_internet, Toast.LENGTH_SHORT).show();
            }
        }
    }
}
