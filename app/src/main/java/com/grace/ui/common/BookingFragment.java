package com.grace.ui.common;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.grace.R;
import com.grace.base.BaseActivity;
import com.grace.databinding.FragmentBookingsBinding;
import com.grace.models.BookingListResponse;
import com.grace.models.GenericResponse;
import com.grace.networks.APIHandler;
import com.grace.networks.APIResponseInterface;
import com.grace.ui.common.adapters.BookingAdapter;
import com.grace.utils.BookingCustomDialog;
import com.grace.utils.ConfirmationDialog;
import com.grace.utils.Constants;
import com.grace.utils.Prefs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class BookingFragment extends Fragment implements APIResponseInterface.OnCallableResponse, Constants.API_CALLS, Constants.PARAMS, Constants {


    private final APIHandler apiHandler;
    private FragmentBookingsBinding binding;
    private LinearLayoutManager linearLayoutManager;
    private BookingAdapter adapter;
    private List<BookingListResponse.DataBean> list;
    private int layoutPosition = 0;

    public BookingFragment() {
        apiHandler = new APIHandler(this);
    }


    BroadcastReceiver receiverBooking = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            getBookingList();
        }
    };


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_bookings, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getActivity() != null) {
            getActivity().registerReceiver(receiverBooking, new IntentFilter(API_CALLS.BOOKING_LIST));

            linearLayoutManager = new LinearLayoutManager(getActivity());
            binding.rvBookings.setLayoutManager(linearLayoutManager);
            list = new ArrayList<>();
            adapter = new BookingAdapter(getActivity(), list, Prefs.getString(ROLE, "").equals("1"));
            binding.rvBookings.setAdapter(adapter);
            adapter.setOnItemClickListener((layoutPosition, check) -> {
                this.layoutPosition = layoutPosition;
                switch (check) {
                    case DETAIL:
                        startActivity(new Intent(getActivity(), HolderActivity.class)
                                .putExtra(NAVIGATE_SCREENS.SCREEN, NAVIGATE_SCREENS.BOOKING_DETAIL_SCREEN)
                                .putExtra(BOOKING_ID, list.get(layoutPosition).getBookingId()));

                        break;
                    case ACCEPT:

                        ConfirmationDialog.getInstance().setParams(this, "Do you want to accept this booking?", "No", "Yes");
                        ConfirmationDialog.getInstance().show(getActivity().getSupportFragmentManager(), "");

                        break;
                }


            });
            getBookingList();
        }

    }

    private void bookingSuccessDialog() {
        if (getActivity() != null) {
            FragmentManager ft = getChildFragmentManager();
            BookingCustomDialog bookingSuccessDialog = new BookingCustomDialog();
            bookingSuccessDialog.setListener(this);
            bookingSuccessDialog.show(ft, BookingCustomDialog.class.getSimpleName());
        }
    }


    private void getBookingList() {
        if (getActivity() != null) {
            if (((BaseActivity) getActivity()).isInternetAvailable(getActivity())) {
                binding.includeLoader.setVisibility(View.VISIBLE);

                Map<String, String> params = new HashMap<>();
                params.put(USER_ID, Prefs.getString(USER_ID, ""));
                params.put(USER_TYPE, Prefs.getString(ROLE, ""));

                apiHandler.getBookingList(params, BOOKING_LIST);
            } else {
                Toast.makeText(getActivity(), R.string.error_internet, Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onSuccess(Response response, String api) {
        binding.includeLoader.setVisibility(View.GONE);
        switch (api) {
            case BOOKING_LIST:
                if (response.isSuccessful()) {
                    BookingListResponse bookingListResponse = (BookingListResponse) response.body();
                    if (bookingListResponse != null) {
                        switch (bookingListResponse.getStatus()) {
                            case STATUS_200:
                                binding.includeNoRecord.setVisibility(View.GONE);
                                list = bookingListResponse.getData();
                                adapter.updateList(list);
                                break;
                            case STATUS_0:
                                binding.includeNoRecord.setVisibility(View.VISIBLE);
                                adapter.updateList(new ArrayList<>());
                                break;
                        }

                    } else {
                        binding.includeNoRecord.setVisibility(View.VISIBLE);
                        adapter.updateList(new ArrayList<>());
                    }
                }
                break;

            case ACCEPT_BOOKING:
                if (response.isSuccessful()) {
                    GenericResponse genericResponse = (GenericResponse) response.body();
                    if (genericResponse != null) {
                        Snackbar.make(binding.getRoot(), genericResponse.getMessage(), Snackbar.LENGTH_SHORT).show();
                        switch (genericResponse.getStatus()) {
                            case STATUS_200:
                                getBookingList();
                                break;
                            case STATUS_0:

                                break;
                        }
                    }
                }
                break;
        }
    }

    @Override
    public void onFailure(Throwable t, String api) {
        if (getActivity() != null) {
            t.getLocalizedMessage();
            binding.includeLoader.setVisibility(View.GONE);
        }
    }

    public void navigate(int value) {
        if (getActivity() != null) {
            switch (value) {
                case 0:
                    startActivity(new Intent(getActivity(), HolderActivity.class)
                            .putExtra(NAVIGATE_SCREENS.SCREEN, NAVIGATE_SCREENS.PAYMENT_SCREEN)
                            .putExtra(BOOKING_ID, list.get(layoutPosition).getBookingId())
                            .putExtra(AMOUNT, list.get(layoutPosition).getTotalAmount().replace("$", "")));
                    break;
            }
        }
    }

    public void confirm() {
        acceptBooking();
    }

    private void acceptBooking() {
        if (getActivity() != null) {
            if (((BaseActivity) getActivity()).isInternetAvailable(getActivity())) {
                binding.includeLoader.setVisibility(View.VISIBLE);
                Map<String, String> params = new HashMap<>();
                params.put(BOOKING_ID, list.get(layoutPosition).getBookingId());
                params.put(ACCEPT, "1");

                apiHandler.acceptBooking(params, ACCEPT_BOOKING);
            } else {
                Toast.makeText(getActivity(), R.string.error_internet, Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void cancel() {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (getActivity() != null) {
            getActivity().unregisterReceiver(receiverBooking);
        }
    }
}
