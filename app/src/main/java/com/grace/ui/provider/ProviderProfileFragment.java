package com.grace.ui.provider;


import android.app.ActivityOptions;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.grace.R;
import com.grace.base.BaseActivity;
import com.grace.databinding.FragmentProviderProfileBinding;
import com.grace.models.ExpertiseResponse;
import com.grace.models.GetProfileResponse;
import com.grace.models.UpdateProfileResponse;
import com.grace.models.UserDataResponse;
import com.grace.networks.APIHandler;
import com.grace.networks.APIResponseInterface;
import com.grace.ui.common.HolderActivity;
import com.grace.utils.Constants;
import com.grace.utils.FileUtils;
import com.grace.utils.PermissionsAndroid;
import com.grace.utils.Prefs;
import com.grace.utils.Utils;
import com.grace.utils.Validations;
import com.kbeanie.multipicker.api.CameraImagePicker;
import com.kbeanie.multipicker.api.ImagePicker;
import com.kbeanie.multipicker.api.Picker;
import com.kbeanie.multipicker.api.callbacks.ImagePickerCallback;
import com.kbeanie.multipicker.api.entity.ChosenImage;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.apptik.widget.multiselectspinner.MultiSelectSpinner;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProviderProfileFragment extends Fragment implements ImagePickerCallback, View.OnClickListener,
        APIResponseInterface.OnCallableResponse, Constants.API_CALLS, Constants.PARAMS, Constants.NAVIGATE_SCREENS {

    FragmentProviderProfileBinding binding;

    private final APIHandler apiHandler;
    private String profileImagePath;
    private Place place;
    private int PLACE_PICKER_REQUEST = 1;
    private CameraImagePicker cameraPicker;
    private ImagePicker imagePicker;
    private String pickerPath;
    private boolean cameraSelected = false;
    private boolean gallerySelected = false;
    private boolean isImageSelected = false;

    private String mSelectedExpertise = "";
    private String expertiseName = "";
    private List<ExpertiseResponse.DataBean> listExpertise;
    private boolean[] arraySelectionExpertise;


    public MultiSelectSpinner.MultiSpinnerListener expertiseSpinnerListener = new MultiSelectSpinner.MultiSpinnerListener() {

        public void onItemsSelected(boolean[] selected) {
            mSelectedExpertise = "";
            expertiseName = "";
            for (int i = 0; i < selected.length; i++) {
                if (selected[i]) {
                    if (mSelectedExpertise.trim().isEmpty()) {
                        mSelectedExpertise = String.valueOf(listExpertise.get(i).getExpertiseId());
                        expertiseName = listExpertise.get(i).getName();
                    } else {
                        mSelectedExpertise = mSelectedExpertise + "," + String.valueOf(listExpertise.get(i).getExpertiseId());
                        expertiseName = expertiseName + "," + listExpertise.get(i).getName();
                    }
                }
            }

            if (getActivity() != null) {
                if (mSelectedExpertise.isEmpty()) {
                    mSelectedExpertise = String.valueOf(listExpertise.get(0).getExpertiseId());
                    expertiseName = listExpertise.get(0).getName();
                    arraySelectionExpertise[0] = true;
                    binding.spinnerExpertise.setAllCheckedText(expertiseName);
                    binding.spinnerExpertise.selectItem(0, arraySelectionExpertise[0]);
                } else {
                    if (binding.spinnerExpertise.isSelectAll())
                        binding.spinnerExpertise.setAllCheckedText(expertiseName);
                }
            }
        }
    };
    private ArrayAdapter<String> expertiseAdapter;
    private String[] detailExpertise;
    private String selectedExpertiseName = "";
    private String userID = "", role = "";
    private MenuItem save, edit;
    private String longitude = "", latitude = "";
    private boolean isDashboard = false;

    public ProviderProfileFragment() {
        // Required empty public constructor
        apiHandler = new APIHandler(this);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            isDashboard = getArguments().getBoolean(getString(R.string.isDashboard), false);
            userID = getArguments().getString(USER_ID, "");
            role = getArguments().getString(ROLE, "");
        }

        if (isDashboard)
            setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_provider_profile, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey("picker_path")) {
                pickerPath = savedInstanceState.getString("picker_path");
            }
        }


        initUI();
        setPrefData();
        getExpertise();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_profile, menu);
        edit = menu.findItem(R.id.action_edit);
        save = menu.findItem(R.id.action_save);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_edit:
                updateUI(true);
                edit.setVisible(false);
                save.setVisible(true);
                break;

            case R.id.action_save:
                updateUI(false);
                save.setVisible(false);
                edit.setVisible(true);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void updateUI(boolean isEdit) {
        if (isEdit) {
            binding.etFullName.setEnabled(true);
            binding.etSalonAddress.setEnabled(true);
            binding.etSalonName.setEnabled(true);
            binding.btnUpdate.setEnabled(true);
            binding.fabPicker.setEnabled(true);

            binding.fabPicker.setOnClickListener(this);
            binding.etSalonAddress.setOnClickListener(this);
            binding.btnUpdate.setOnClickListener(this);
            binding.ivSalon.setOnClickListener(this);
            binding.spinnerExpertise.setListener(expertiseSpinnerListener);


        } else {
            binding.etFullName.setEnabled(false);
            binding.etSalonAddress.setEnabled(false);
            binding.etSalonName.setEnabled(false);
            binding.btnUpdate.setEnabled(false);
            binding.fabPicker.setEnabled(false);

            binding.fabPicker.setOnClickListener(null);
            binding.etSalonAddress.setOnClickListener(null);
            binding.btnUpdate.setOnClickListener(null);
            binding.ivSalon.setOnClickListener(null);
            binding.spinnerExpertise.setListener(null);

        }
    }

    private void setPrefData() {
        binding.etEmail.setText(Prefs.getString(EMAIL, ""));
        binding.etPhone.setText(Prefs.getString(PHONE, ""));
        binding.etSalonAddress.setText(Prefs.getString(ADDRESS, ""));
        binding.etFullName.setText(Prefs.getString(FULL_NAME, ""));
        binding.etSalonName.setText(Prefs.getString(SALON_NAME, ""));
        latitude = Prefs.getString(LATITUDE, "");
        longitude = Prefs.getString(LONGITUDE, "");

        if (!TextUtils.isEmpty(Prefs.getString(SALON_IMAGE, ""))) {
            binding.progress.setVisibility(View.VISIBLE);
            Picasso.get().load(Prefs.getString(SALON_IMAGE, "")).resize(300,300).into(binding.ivSalon, new Callback() {
                @Override
                public void onSuccess() {
                    binding.progress.setVisibility(View.GONE);
                }

                @Override
                public void onError(Exception e) {
                    e.printStackTrace();
                    binding.progress.setVisibility(View.GONE);
                }

            });
        }


        binding.includeLoader.setVisibility(View.GONE);
    }

    private void getExpertise() {
        if (Utils.getInstance().isInternetAvailable(getActivity())) {
            apiHandler.getExpertise(EXPERTISE_LIST);
        } else {
            Utils.getInstance().showSnackBar(getView(), getString(R.string.error_internet));
        }
    }

    private void initUI() {


        binding.btnUpdate.setOnClickListener(this);
        binding.ivSalon.setOnClickListener(this);
        binding.etSalonAddress.setOnClickListener(this);
        binding.fabPicker.setOnClickListener(this);
        binding.spinnerExpertise.setListener(expertiseSpinnerListener);

    }

    //  @TargetApi(Build.VERSION_CODES.M)
    private void checkCameraPermission() {
        boolean isExternalStorage = PermissionsAndroid.getInstance().checkCameraPermission(getActivity());
        if (!isExternalStorage) {
            PermissionsAndroid.getInstance().requestForCameraPermission(this);
        } else if (cameraSelected) {
            takePicture();
        } else if (gallerySelected) {
            pickImageSingle();
        }
    }

    private void takePicture() {
        cameraPicker = new CameraImagePicker(getActivity());
//        cameraPicker.setCacheLocation(CacheLocation.INTERNAL_APP_DIR);
        cameraPicker.setImagePickerCallback(this);
        cameraPicker.shouldGenerateMetadata(true);
        cameraPicker.shouldGenerateThumbnails(true);
        pickerPath = cameraPicker.pickImage();
    }

    private void pickImageSingle() {
        imagePicker = new ImagePicker(getActivity());
        imagePicker.shouldGenerateMetadata(true);
        imagePicker.shouldGenerateThumbnails(true);
        imagePicker.setImagePickerCallback(this);
        imagePicker.pickImage();
    }

    @Override
    public void onImagesChosen(List<ChosenImage> list) {
        if (getActivity() != null) {
            String extension = "";
            int i = list.get(0).getDisplayName().lastIndexOf('.');
            if (i > 0) {
                extension = list.get(0).getDisplayName().substring(i + 1);
            }
            if (!extension.equals("png") && !extension.equals("jpg") && !extension.equals("jpeg")) {
                ((BaseActivity) getActivity()).showSnackBar(getView(), "you can choose only images.");
                return;
            }
            ChosenImage image = list.get(0);
            isImageSelected = true;
            profileImagePath = FileUtils.getPath(getActivity(), Uri.parse(image.getQueryUri()));
            binding.progress.setVisibility(View.VISIBLE);
            Picasso.get().load(new File(profileImagePath)).resize(300,300).into(binding.ivSalon, new Callback() {
                @Override
                public void onSuccess() {
                    binding.progress.setVisibility(View.GONE);
                }

                @Override
                public void onError(Exception e) {
                    binding.progress.setVisibility(View.GONE);
                }

            });
        }
    }

    @Override
    public void onError(String s) {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (getActivity() != null) {
            if (((BaseActivity) getActivity()).isShowing()) {
                ((BaseActivity) getActivity()).hideProgressDialog();
            }
            if (resultCode == RESULT_OK) {
                if (requestCode == PLACE_PICKER_REQUEST) {
                    place = PlacePicker.getPlace(getActivity(), data);
                    if (place != null) {
                        latitude = String.valueOf(place.getLatLng().latitude);
                        longitude = String.valueOf(place.getLatLng().longitude);
                        binding.etSalonAddress.setText(place.getAddress());
                    }
                } else if (requestCode == Picker.PICK_IMAGE_DEVICE) {
                    if (imagePicker == null) {
                        imagePicker = new ImagePicker(getActivity());
                    }
                    imagePicker.submit(data);
                } else if (requestCode == Picker.PICK_IMAGE_CAMERA) {
                    if (cameraPicker == null) {
                        cameraPicker = new CameraImagePicker(getActivity());
                        cameraPicker.setImagePickerCallback(this);
                        cameraPicker.reinitialize(pickerPath);
                    }
                    cameraPicker.submit(data);
                }
            } else {
                Utils.getInstance().showToast("Request cancelled");
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case 102: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Show option to update profile
                    if (cameraSelected) {
                        takePicture();
                    } else if (gallerySelected) {
                        pickImageSingle();
                    }

                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Utils.getInstance().showToast("Camera/Storage permission Denied");
                }
                return;
            }

        }
    }

    @Override
    public void onClick(View v) {
        if (getActivity() != null) {
            switch (v.getId()) {
                case R.id.fab_picker:
                    // show picker for image capture
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    String[] options = {"Camera", "Gallery", "Cancel"};
                    final Bundle bundle = new Bundle();
                    builder.setTitle("Choose Option")
                            .setItems(options, (dialog, which) -> {
                                // The 'which' argument contains the index position
                                switch (which) {
                                    case 0:
                                        cameraSelected = true;
                                        gallerySelected = false;
                                        checkCameraPermission();
                                        break;
                                    case 1:
                                        cameraSelected = false;
                                        gallerySelected = true;
                                        checkCameraPermission();
                                        break;
                                    case 2:
                                        dialog.dismiss();
                                        break;
                                }
                            });
                    builder.setCancelable(false);
                    builder.show();
                    break;
                case R.id.btn_update:
                    updateProfileData();
                    break;
                case R.id.et_salon_address:
                    //Move to Search Address Fragment
                    ((BaseActivity) getActivity()).showProgressDialog();
                    try {
                        PlacePicker.IntentBuilder placeBuilder = new PlacePicker.IntentBuilder();
                        getActivity().startActivityForResult(placeBuilder.build(getActivity()), PLACE_PICKER_REQUEST);
                    } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
                        e.printStackTrace();
                        ((BaseActivity) getActivity()).hideProgressDialog();
                    }
                    break;
                case R.id.iv_salon:
                    enlargeImage();
                    break;
            }

        }
    }

    private void updateProfileData() {
        if (getActivity() != null) {
            if (Utils.getInstance().isInternetAvailable(getActivity())) {
                String fullName = binding.etFullName.getText().toString().trim();
                String salonName = binding.etSalonName.getText().toString().trim();
                String address = binding.etSalonAddress.getText().toString().trim();
                String response = Validations.getInstance().validateProviderProfileFields(address,
                        mSelectedExpertise,fullName);
                if (response.isEmpty()) {
                    ((BaseActivity) getActivity()).showProgressDialog();
                    Map<String, String> params = new HashMap<>();
                    params.put(USER_ID, userID);
                    params.put(ROLE, role);
                    params.put(FULL_NAME, fullName);
                    params.put(SALON_NAME, salonName);
                    params.put(EXPERTISE, mSelectedExpertise);
                    params.put(ADDRESS, address);
                    params.put(LATITUDE, latitude);
                    params.put(LONGITUDE, longitude);
                    if (isImageSelected && !TextUtils.isEmpty(profileImagePath)) {
                        apiHandler.updateProfileWithPic(params, profileImagePath, UPDATE_PROFILE);
                    } else {
                        apiHandler.updateProfile(params, UPDATE_PROFILE);
                    }
                } else {
                    Utils.getInstance().showSnackBar(getView(), response);
                }
            } else {
                Utils.getInstance().showSnackBar(getView(), getString(R.string.error_internet));
            }
        }
    }

    private void enlargeImage() {
        if (getActivity() != null) {
            if (!TextUtils.isEmpty(profileImagePath)) {


                Intent profilePicIntent = new Intent(getActivity(), HolderActivity.class);
                profilePicIntent.putExtra(SCREEN, VIEW_IMAGE_SCREEN);
                profilePicIntent.putExtra(PIC_TITLE, "Profile Pic");
                profilePicIntent.putExtra(BROWSE_PIC, profileImagePath);
                if (Utils.getInstance().isEqualLollipop()) {


                    Pair<View, String> p1 = Pair.create(binding.ivSalon, TRANSITION_IMAGE);
                    ActivityOptions options =
                            ActivityOptions.makeSceneTransitionAnimation(getActivity(), p1);
                    getActivity().startActivity(profilePicIntent, options.toBundle());
                } else {
                    getActivity().startActivity(profilePicIntent);
                }


            } else if (!TextUtils.isEmpty(Prefs.getString(SALON_IMAGE, ""))) {
                Intent profilePicIntent = new Intent(getActivity(), HolderActivity.class);
                profilePicIntent.putExtra(SCREEN, VIEW_IMAGE_SCREEN);
                profilePicIntent.putExtra(PIC_TITLE, "Profile Pic");
                profilePicIntent.putExtra(BROWSE_PIC, Prefs.getString(SALON_IMAGE, ""));
                if (Utils.getInstance().isEqualLollipop()) {
                    Pair<View, String> p1 = Pair.create(binding.ivSalon, TRANSITION_IMAGE);
                    ActivityOptions options =
                            ActivityOptions.makeSceneTransitionAnimation(getActivity(), p1);
                    getActivity().startActivity(profilePicIntent, options.toBundle());
                } else {
                    getActivity().startActivity(profilePicIntent);
                }
            }
        }
    }

    @Override
    public void onSuccess(Response response, String api) {
        if (getActivity() != null) {
            if (((BaseActivity) getActivity()).isShowing())
                ((BaseActivity) getActivity()).hideProgressDialog();
            switch (api) {
                case UPDATE_PROFILE:
                    if (response.isSuccessful()) {
                        UpdateProfileResponse updateProfileResponse = (UpdateProfileResponse) response.body();
                        if (updateProfileResponse != null) {
                            Toast.makeText(getActivity(), updateProfileResponse.getMessage(), Toast.LENGTH_SHORT).show();

                            UserDataResponse dataObj = updateProfileResponse.getData().get(0);
                            Prefs.putString(USER_ID, dataObj.getUserId());
                            Prefs.putString(EMAIL, dataObj.getEmail());
                            Prefs.putString(ROLE, dataObj.getRole());
                            Prefs.putString(PHONE, dataObj.getPhone());
                            Prefs.putString(EXPERTISE_ID, dataObj.getExpertise());
                            Prefs.putString(SALON_NAME, dataObj.getSalonName());
                            Prefs.putString(LATITUDE, String.valueOf(dataObj.getLatitude()));
                            Prefs.putString(LONGITUDE, String.valueOf(dataObj.getLongitude()));
                            Prefs.putString(SALON_IMAGE, dataObj.getSalonImage());
                            Prefs.putString(FULL_NAME, dataObj.getFullName());
                            Prefs.putString(ADDRESS, dataObj.getAddress());
                            Prefs.putBoolean(IS_LOGIN, true);

                            if (!isDashboard) {
                                startActivity(new Intent(getActivity(), ProviderDashboard.class));
                                getActivity().finish();
                            }else{
                                updateUI(false);
                                save.setVisible(false);
                                edit.setVisible(true);
                            }

                            if (getActivity() instanceof ProviderDashboard) {
                                ((ProviderDashboard) getActivity()).updateData();
                            }
                        }
                    }
                    break;

                case GET_PROFILE:
                    if (response.isSuccessful()) {
                        GetProfileResponse getProfileResponse = (GetProfileResponse) response.body();
                        if (getProfileResponse != null) {
                            Toast.makeText(getActivity(), getProfileResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                    break;

                case EXPERTISE_LIST:
                    if (response.isSuccessful()) {
                        ExpertiseResponse expertiseResponse = (ExpertiseResponse) response.body();
                        if (expertiseResponse != null) {
                            Toast.makeText(getActivity(), expertiseResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            setExpertise(expertiseResponse.getData());
                        }
                    }
                    break;
            }
        }
    }

    private void setExpertise(List<ExpertiseResponse.DataBean> data) {

        if (getActivity() != null) {
            listExpertise = new ArrayList<>();
            if (!listExpertise.isEmpty())
                listExpertise.clear();

            List<String> listExpert = new ArrayList<>();

            listExpertise.addAll(data);
            for (int i = 0; i < listExpertise.size(); i++) {
                listExpert.add(listExpertise.get(i).getName());
            }

            expertiseAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_multiple_choice, listExpert);
            binding.spinnerExpertise.setListAdapter(expertiseAdapter).setAllCheckedText(selectedExpertiseName);

            if (!listExpertise.isEmpty()) {
                arraySelectionExpertise = new boolean[listExpertise.size()];
                for (int j = 0; j < listExpertise.size(); j++) {
                    if (detailExpertise != null) {

                        for (int i = 0; i < detailExpertise.length; i++) {
                            if (detailExpertise[i].equals(String.valueOf(listExpertise.get(j).getExpertiseId()))) {
                                arraySelectionExpertise[j] = true;
                                if (expertiseName.trim().isEmpty())
                                    expertiseName = String.valueOf(listExpertise.get(j).getName());
                                else
                                    expertiseName = expertiseName + "," + String.valueOf(listExpertise.get(j).getName());
                                if (mSelectedExpertise.trim().isEmpty())
                                    mSelectedExpertise = String.valueOf(listExpertise.get(j).getExpertiseId());
                                else
                                    mSelectedExpertise = mSelectedExpertise + "," + String.valueOf(listExpertise.get(j).getExpertiseId());
                            }
                        }
                    }

                }
                switch (expertiseName) {
                    case "":
                        mSelectedExpertise = String.valueOf(listExpertise.get(0).getExpertiseId());
                        expertiseName = listExpertise.get(0).getName();
                        arraySelectionExpertise[0] = true;
                        break;
                }


                binding.spinnerExpertise.setAllCheckedText(expertiseName);
                for (int k = 0; k < arraySelectionExpertise.length; k++) {
                    System.out.println(k + "K----boolean" + arraySelectionExpertise[k]);
                    binding.spinnerExpertise.selectItem(k, arraySelectionExpertise[k]);
                }
            }
        }
    }

    @Override
    public void onFailure(Throwable t, String api) {
        if (getActivity() != null) {
            Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            if (((BaseActivity) getActivity()).isShowing())
                ((BaseActivity) getActivity()).hideProgressDialog();
        }
    }
}
