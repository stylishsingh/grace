package com.grace.ui.provider;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.google.firebase.messaging.FirebaseMessaging;
import com.grace.R;
import com.grace.base.BaseActivity;
import com.grace.ui.common.BookingFragment;
import com.grace.ui.common.LoginSignUpActivity;
import com.grace.ui.customer.CustomerProfileFragment;
import com.grace.utils.Constants;
import com.grace.utils.Prefs;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProviderDashboard extends BaseActivity
        implements  Constants.PARAMS, View.OnClickListener {

    private DrawerLayout drawer;
    private TextView tvName, tvEmail;
    private String currentScreen = "";
    private Toolbar toolbar;
    private CircleImageView ivPic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_provider_dashboard);

        FirebaseMessaging.getInstance().subscribeToTopic(getString(R.string.app_name) + Prefs.getString(USER_ID, ""));

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle("");
        toolbar.setTitle("Bookings");

        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        findViewById(R.id.nav_bookings).setOnClickListener(this);
        findViewById(R.id.nav_profile).setOnClickListener(this);
        findViewById(R.id.rl_logout).setOnClickListener(this);

        tvName = findViewById(R.id.tv_name);
        ivPic = findViewById(R.id.iv_profile);

        updateData();



        if (!currentScreen.equalsIgnoreCase(BookingFragment.class.getSimpleName())) {
            Bundle bundle = new Bundle();
            bundle.putBoolean(getString(R.string.isCustomer), false);
            toolbar.setTitle("Bookings");
            currentScreen = BookingFragment.class.getSimpleName();
            navigateToWithBundle(R.id.container, new BookingFragment(), currentScreen,
                    false, bundle);
        }

    }

    @Override
    public void onBackPressed() {
        drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        (getSupportFragmentManager().findFragmentById(R.id.container)).
                onActivityResult(requestCode, resultCode, data);
    }

    public void updateData() {
        tvName.setText(Prefs.getString(FULL_NAME, ""));

        if (!TextUtils.isEmpty(Prefs.getString(Constants.PARAMS.SALON_IMAGE, "")))
            Picasso.get().load(Prefs.getString(Constants.PARAMS.SALON_IMAGE, "")).resize(80,80).into(ivPic, new Callback() {
                @Override
                public void onSuccess() {
//                binding.progress.setVisibility(View.GONE);
                }

                @Override
                public void onError(Exception e) {
//                binding.progress.setVisibility(View.GONE);
                }

            });
    }

    @Override
    public void onClick(View v) {
        Bundle bundle = new Bundle();
        switch (v.getId()) {
            case R.id.nav_bookings:
                if (!currentScreen.equalsIgnoreCase(BookingFragment.class.getSimpleName())) {
                    bundle.putBoolean(getString(R.string.isCustomer), false);
                    toolbar.setTitle("Bookings");
                    currentScreen = BookingFragment.class.getSimpleName();
                    navigateToWithBundle(R.id.container, new BookingFragment(), currentScreen,
                            false, bundle);
                }
                break;
            case R.id.nav_profile:
                toolbar.setTitle("Profile");
                if (!currentScreen.equalsIgnoreCase(CustomerProfileFragment.class.getSimpleName())) {
                    bundle.putBoolean(getString(R.string.isDashboard), true);
                    bundle.putString(USER_ID, Prefs.getString(USER_ID, ""));
                    bundle.putString(ROLE, Prefs.getString(ROLE, ""));
                    currentScreen = ProviderProfileFragment.class.getSimpleName();
                    navigateToWithBundle(R.id.container, new ProviderProfileFragment(), currentScreen, false, bundle);
                }
                break;
            case R.id.rl_logout:
                FirebaseMessaging.getInstance().unsubscribeFromTopic(getString(R.string.app_name) + Prefs.getString(USER_ID, ""));
                Prefs.clear();
                startActivity(new Intent(this, LoginSignUpActivity.class));
                finish();
                break;
        }

        drawer.closeDrawer(GravityCompat.START);

    }
}
