package com.grace.ui.splash;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.grace.base.BaseActivity;
import com.grace.ui.common.HolderActivity;
import com.grace.ui.common.LoginSignUpActivity;
import com.grace.ui.customer.CustomerDashboard;
import com.grace.ui.provider.ProviderDashboard;
import com.grace.utils.Constants;
import com.grace.utils.Prefs;

public class SplashActivity extends BaseActivity implements Constants.PARAMS {

    Handler handler;

    Runnable runnable = () -> {

        if (Prefs.getBoolean(IS_LOGIN, false)) {

            if (Prefs.getString(ROLE, "").equalsIgnoreCase("1")) {
                Bundle bundle = getIntent().getExtras();
                System.out.println("SplashActivity.BUNDLE---" + bundle);
                if (bundle != null && bundle.containsKey(Constants.PARAMS.BOOKING_ID)) {
                    Intent intent = new Intent(getApplicationContext(), HolderActivity.class);
                    intent.putExtra(Constants.NAVIGATE_SCREENS.SCREEN, Constants.NAVIGATE_SCREENS.BOOKING_DETAIL_SCREEN);
                    intent.putExtra(Constants.PARAMS.BOOKING_ID, bundle.getString(Constants.PARAMS.BOOKING_ID));
                    startActivity(intent);
                } else
                    startActivity(new Intent(SplashActivity.this, CustomerDashboard.class));
            } else if (Prefs.getString(ROLE, "").equalsIgnoreCase("2")) {
                Bundle bundle = getIntent().getExtras();
                System.out.println("SplashActivity.BUNDLE---" + bundle);
                if (bundle != null && bundle.containsKey(Constants.PARAMS.BOOKING_ID)) {
                    Intent intent = new Intent(getApplicationContext(), HolderActivity.class);
                    intent.putExtra(Constants.NAVIGATE_SCREENS.SCREEN, Constants.NAVIGATE_SCREENS.BOOKING_DETAIL_SCREEN);
                    intent.putExtra(Constants.PARAMS.BOOKING_ID, bundle.getString(Constants.PARAMS.BOOKING_ID));
                    startActivity(intent);
                } else
                    startActivity(new Intent(SplashActivity.this, ProviderDashboard.class));

            }
        } else {
            startActivity(new Intent(SplashActivity.this, LoginSignUpActivity.class));
        }
        finish();


    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        handler = new Handler();
//        Crashlytics.getInstance().crash(); // Force a crash
    }


    @Override
    protected void onResume() {
        super.onResume();
        handler.postDelayed(runnable, 2000);
    }

    @Override
    protected void onPause() {
        super.onPause();
        System.out.println("SplashActivity.onPause");
        if (handler != null) handler.removeCallbacks(runnable);
    }
}
