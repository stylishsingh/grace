package com.grace.utils;

/**
 * @author Amandeep Singh
 * @date 27/07/2018
 */
public enum Events {
    LIKE,
    COMMENT,
    MORE_INFO,
    STORE_LOCATION,
    OCCASION,
    DRESSING,
    NATIONALITY,
    CULTURAL_BACKGROUND,
    FOLLOW_UNFOLLOW,
    BLOCK,
    CANCEL,
    FOLLOW,
    UN_FOLLOW,
    VIEW_PROFILE
}
