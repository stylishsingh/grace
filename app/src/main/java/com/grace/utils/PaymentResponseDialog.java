package com.grace.utils;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import com.grace.R;
import com.grace.databinding.DialogPaymentResponseBinding;
import com.grace.ui.customer.PaymentFragment;

public class PaymentResponseDialog extends DialogFragment implements View.OnClickListener {

    DialogPaymentResponseBinding binding;
    private Fragment fragment;
    private int status = 0;

    public void setListener(Fragment fragment) {
        this.fragment = fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_payment_response, container, false);

        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getActivity() != null && getDialog() != null && getDialog().getWindow() != null) {

            getDialog().setTitle("Confirmation");
            setCancelable(false);
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

            getDialog().getWindow().setSoftInputMode(
                    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

            switch (status) {
                case 0:
                    binding.ivPayment.setImageResource(R.drawable.ic_checked_60);
                    binding.tvGreet.setText(R.string.payment_successful);
                    break;
                case 1:
                    binding.ivPayment.setImageResource(R.drawable.ic_failure);
                    binding.tvGreet.setText(R.string.payment_failure);
                    break;
                case 2:
                    binding.ivPayment.setImageResource(R.drawable.ic_failure);
                    binding.tvGreet.setText(R.string.payment_cancel);
                    break;
            }


            binding.btnDone.setOnClickListener(this);

        }

    }

    @Override
    public void onClick(View v) {
        getDialog().dismiss();
        switch (v.getId()) {
            case R.id.btn_done:
                if (fragment instanceof PaymentFragment) {
                    PaymentFragment paymentFragment = (PaymentFragment) fragment;
                    paymentFragment.paymentStatus(status);
                }
                break;
        }
    }

    public void setParams(int status) {
        this.status = status;
    }
}
