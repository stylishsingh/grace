package com.grace.utils;

public interface Constants {
    String DEV_API_URL = "http://graceapp.co.in/";
    int STATUS_200 = 200;
    int STATUS_201 = 201;
    int STATUS_0 = 0;
    String GPS_RECEIVER = "GPS_RECEIVER";
    String IS_GPS_ENABLED = "isGPSEnabled";
    String DETAIL = "detail";
    String ACCEPT = "accept";
    String BOOKING_ADDRESS = "booking_address";


    interface API_CALLS {
        String LOGIN = "login/login.php";
        String SIGN_UP = "signup/signup.php";
        String SUBCATEGORIES_LIST = "serviceprovider/get-expertise-subcategories-list.php";
        String EXPERTISE_LIST = "serviceprovider/get-expertise-list.php";
        String SERVICE_PROVIDER_LIST = "serviceprovider/get-service-providers-list.php";
        String UPDATE_PROFILE = "serviceprovider/updateprofile.php";
        String GET_PROFILE = "serviceprovider/getprofile.php";
        String BOOK_APPOINTMENT = "booking/booking-appointment.php";
        String FORGOT_PASSWORD = "forgot-password.php";
        String BOOKING_LIST = "booking/get-bookings.php";
        String RESET_PASSWORD = "reset-password.php";
        String FEEDBACk = "serviceprovider/feedback.php";
        String JOB_STATUS_CHANGED = "booking/job-status-changed.php";
        String ACCEPT_BOOKING = "booking/accept-booking.php";
        String BOOKING_PAYMENT_PROCESS = "booking/payment/paymentProcess.php";
        String BOOKING_DETAILS = "booking/getBookingDetail.php";
    }

    interface PARAMS {

        /*API Params*/
        String DATE = "DATE";
        String USER_ID = "user_id";
        String OTP = "otp";
        String EMAIL = "email";
        String PASSWORD = "password";
        String NEW_PASSWORD = "new_password";
        String PHONE = "phone";
        String ROLE = "role";
        String EXPERTISE_ID = "expertise_id";
        String EXPERTISE_NAME = "expertise_name";
        String SALON_ID = "salon_id";
        String CUSTOMER_ID= "customer_id";
        String TIME = "time";
        String MEN = "men";
        String WOMEN = "women";
        String KIDS = "kids";
        String TOTAL_AMOUNT = "total_amount";
        String SERVICE_PROVIDER_ID = "service_provider_id";
        String LATITUDE = "latitude";
        String LONGITUDE = "longitude";
        String RATING = "rating";
        String COMMENT = "comment";
        String AMOUNT = "amount";
        String BOOKING_ID = "booking_id";
        String JOB_STATUS= "job_status";
        String SALON_IMAGE = "salon_image";
        String SALON_NAME = "salon_name";
        String EXPERTISE = "expertise";
        String FULL_NAME = "full_name";
        String ADDRESS = "address";
        String USER_TYPE = "user_type";
        String IS_LOGIN = "is_login";


        /*Other Params*/
        String PIC_TITLE = "PIC_TITLE";
        String BROWSE_PIC = "BROWSE_PIC";
        String TRANSITION_IMAGE = "transition_image";
        String SALON_INFO = "SALON_INFO";
        String PROVIDER_ID = "PROVIDER_ID";
        String PROVIDER_NAME = "PROVIDER_NAME";
        String BOOKING_INFO = "BOOKING_INFO";
        String PAYMENT_STATUS ="PAYMENT_STATUS";

    }

    interface NAVIGATE_SCREENS {
        String SCREEN = "SCREEN";
        String EXPERTISE_SCREEN = "EXPERTISE_SCREEN";
        String SALON_DETAIL_SCREEN = "SALON_DETAIL_SCREEN";
        String PROVIDER_SCREEN = "PROVIDER_SCREEN";
        String BOOK_APPOINTMENT = "BOOK_APPOINTMENT";
        String VIEW_IMAGE_SCREEN = "VIEW_IMAGE_SCREEN";
        String PAYMENT_SCREEN = "PAYMENT_SCREEN";
        String BOOKING_DETAIL_SCREEN = "BOOKING_DETAIL_SCREEN";
    }

}
