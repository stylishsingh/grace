package com.grace.utils;

import android.text.TextUtils;

public class Validations {

    private static Validations instance = null;

    private Validations() {

    }

    public static Validations getInstance() {
        if (instance == null) return new Validations();
        else return instance;
    }

    public String validateLoginFields(String email, String password) {
        if (email.isEmpty() && password.isEmpty()) {
            return "Please enter all fields";
        } else if (email.isEmpty()) {
            return "Please enter email";
        } else if (password.isEmpty()) {
            return "Please enter password";
        }
        return "";
    }

    public String validateSignUpFields(String email, String phone, String role, String password, String confirmPassword) {
        if (email.isEmpty() && phone.isEmpty() && role.isEmpty() && password.isEmpty() && confirmPassword.isEmpty()) {
            return "Please enter all fields";
        } else if (email.isEmpty()) {
            return "Please enter email";
        } else if (phone.isEmpty()) {
            return "Please enter phone";
        } else if (role.isEmpty()) {
            return "Please select role";
        } else if (password.isEmpty()) {
            return "Please enter password";
        } else if (confirmPassword.isEmpty()) {
            return "Please enter confirm password";
        }else if (!password.equals(confirmPassword)) {
            return "Password and confirm password don't match";
        }else if (phone.length()<10||phone.length()>10){
            return "Please enter 10 digit number";
        }
        return "";
    }

    public String validateProviderProfileFields(String place, String expertise, String fullName) {
        if (TextUtils.isEmpty(fullName)) {
            return "Please enter full name";
        } else if (TextUtils.isEmpty(expertise)) {
            return "Please select expertise";
        } else if (TextUtils.isEmpty(place)) {
            return "Please select address";
        }
        return "";
    }


    public String validateCustomerProfileFields(String place, String fullName) {
        if (TextUtils.isEmpty(fullName)) {
            return "Please enter full name";
        } else if (TextUtils.isEmpty(place)) {
            return "Please select address";
        }
        return "";
    }

}
