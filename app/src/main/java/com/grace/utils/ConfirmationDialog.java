package com.grace.utils;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;

import com.grace.ui.common.BookingFragment;

import static okhttp3.internal.Internal.instance;

public class ConfirmationDialog extends DialogFragment {

    public static ConfirmationDialog instance = null;

    private String header = "";
    private String positiveButton = "", negativeButton = "";
    private Fragment fragment;

    public synchronized static ConfirmationDialog getInstance() {
        if (instance == null)
            return instance = new ConfirmationDialog();
        else
            return instance;
    }

    public void setParams(Fragment fragment, String header, String negativeButton, String positiveButton) {
        this.fragment = fragment;
        this.header = header;
        this.positiveButton = positiveButton;
        this.negativeButton = negativeButton;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(header)
                .setPositiveButton(positiveButton, (dialog, id) -> {
                    if (fragment instanceof BookingFragment) {
                        BookingFragment bookingFragment = (BookingFragment) fragment;
                        bookingFragment.confirm();
                    }
                })
                .setNegativeButton(negativeButton, (dialog, id) -> {
                    if (fragment instanceof BookingFragment) {
                        BookingFragment bookingFragment = (BookingFragment) fragment;
                        bookingFragment.cancel();
                    }
                });

        return builder.create();

    }
}
