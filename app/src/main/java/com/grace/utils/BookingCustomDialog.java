package com.grace.utils;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import com.grace.R;
import com.grace.databinding.DialogBookingSuccessBinding;
import com.grace.ui.common.BookingFragment;

public class BookingCustomDialog extends DialogFragment implements View.OnClickListener {

    DialogBookingSuccessBinding binding;
    Fragment fragment;

    public void setListener(Fragment fragment) {
        this.fragment = fragment;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_booking_success, container, false);

        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getActivity() != null && getDialog() != null && getDialog().getWindow() != null) {

            getDialog().setTitle("Confirmation");
//            setCancelable(false);
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

            getDialog().getWindow().setSoftInputMode(
                    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);


            binding.btnPay.setOnClickListener(this);

        }

    }

    @Override
    public void onClick(View v) {
        getDialog().dismiss();
        switch (v.getId()) {
            case R.id.btn_pay:
                if (fragment instanceof BookingFragment) {
                    BookingFragment bookingFragment = (BookingFragment) fragment;
                    bookingFragment.navigate(0);
                }
                break;
        }
    }

}
