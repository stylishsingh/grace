package com.grace.utils;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.grace.R;
import com.grace.base.BaseActivity;
import com.grace.databinding.DialogFeedbackBinding;
import com.grace.models.GenericResponse;
import com.grace.networks.APIHandler;
import com.grace.networks.APIResponseInterface;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Response;


public class FeedbackDialog extends DialogFragment implements View.OnClickListener, APIResponseInterface.OnCallableResponse,
        Constants {

    private final APIHandler apiHandler;
    DialogFeedbackBinding binding;
    Fragment fragment;
    String bookingID, salonID, serviceProviderID, userID;
    private float rating = 0f;

    public FeedbackDialog() {
        apiHandler = new APIHandler(this);
    }

    public void setListener(Fragment fragment, String bookingID, String salonID, String serviceProviderID, String userID) {
        this.fragment = fragment;
        this.userID = userID;
        this.salonID = salonID;
        this.bookingID = bookingID;
        this.serviceProviderID = serviceProviderID;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_feedback, container, false);

        return binding.getRoot();

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getActivity() != null && getDialog() != null && getDialog().getWindow() != null) {

            setCancelable(false);
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

            getDialog().getWindow().setSoftInputMode(
                    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);


            binding.btnDone.setOnClickListener(this);
            binding.btnCancel.setOnClickListener(this);
            binding.rating.setOnRatingBarChangeListener((ratingBar, rating, fromUser) -> {
                if (rating == 0.0)
                    binding.rating.setRating(1);

            });

        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_done:
                sendFeedback();
                break;
            case R.id.btn_cancel:
                dismiss();
                break;
        }
    }

    public void sendFeedback() {
        if (getActivity() != null) {
            if (((BaseActivity) getActivity()).isInternetAvailable(getActivity())) {
                binding.includeLoader.setVisibility(View.VISIBLE);
                Map<String, String> params = new HashMap<>();
                params.put(PARAMS.BOOKING_ID, bookingID);
                params.put(PARAMS.SERVICE_PROVIDER_ID, serviceProviderID);
                params.put(PARAMS.CUSTOMER_ID, userID);
                params.put(PARAMS.SALON_ID, salonID);
                params.put(PARAMS.COMMENT, binding.etComments.getText().toString().trim());
                params.put(PARAMS.RATING, String.valueOf(binding.rating.getRating()));
                apiHandler.sendFeedback(params, API_CALLS.FEEDBACk);
            } else {
                Toast.makeText(getActivity(), R.string.error_internet, Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onSuccess(Response response, String api) {
        binding.includeLoader.setVisibility(View.GONE);
        switch (api) {
            case API_CALLS.FEEDBACk:
                if (response.isSuccessful()) {
                    GenericResponse genericResponse = (GenericResponse) response.body();
                    if (genericResponse != null) {
// Snackbar.make(binding.getRoot(), genericResponse.getMessage(), Snackbar.LENGTH_SHORT).show();
                    Toast.makeText(getContext(), genericResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    dismiss();
                }
        }


        break;
    }

}

    @Override
    public void onFailure(Throwable t, String api) {
        if (getActivity() != null) {
            t.getLocalizedMessage();
            binding.includeLoader.setVisibility(View.GONE);
        }
    }
}
