package com.grace.utils;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.support.v4.app.Fragment;

import com.grace.ui.customer.BookAppointment;

import java.util.Calendar;

public class CalendarPicker {

    private static CalendarPicker calendarInstance = null;
    private Fragment fragment;

    private CalendarPicker() {

    }

    public static CalendarPicker getInstance() {
        if (calendarInstance == null)
            return new CalendarPicker();
        else
            return calendarInstance;
    }


    public void showCalendar(final Context context, Fragment fragment) {

        this.fragment = fragment;

        final Calendar calendar = Calendar.getInstance();


        int year, month, day;

        DatePickerDialog datepickerDialog = null;
        try {
            year = calendar.get(Calendar.YEAR);
            month = calendar.get(Calendar.MONTH);
            day = calendar.get(Calendar.DAY_OF_MONTH);


            datepickerDialog = new DatePickerDialog(context, (view, selectedYear, selectedMonth, selectedDay) -> {
                String selectedDate = String.valueOf(selectedDay) + "/" + (selectedMonth + 1) + "/" + selectedYear;
                TimePicker(context, selectedDate);
            }, year, month, day);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

        // Calendar Minimum Range
        Calendar calendarMinDate = Calendar.getInstance();
        calendarMinDate.add(Calendar.DATE, 0);

        if (datepickerDialog != null) {
            datepickerDialog.getDatePicker().setMinDate(calendarMinDate.getTimeInMillis() - 1000);
            datepickerDialog.show();
        }
    }

    private void TimePicker(final Context context, final String selectedDate) {
        TimePickerDialog timePickerDialog = null;
        try {
            final Calendar c = Calendar.getInstance();
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);

            timePickerDialog = new TimePickerDialog(context, (view, selectedHour, selectedMinute) -> {
                String selectedTime = String.valueOf(selectedHour) + ":" + String.valueOf(selectedMinute);
                if (fragment instanceof BookAppointment) {
                    BookAppointment bookAppointment = (BookAppointment) fragment;
                    bookAppointment.setDate(selectedDate, selectedTime);
                }

            }, hour, minute, true);


        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        if (timePickerDialog != null)
            timePickerDialog.show();
    }

}