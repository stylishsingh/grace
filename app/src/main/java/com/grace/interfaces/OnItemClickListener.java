package com.grace.interfaces;

public interface OnItemClickListener {
    void onItemClick(int layoutPosition, String check);
}

