package com.grace.gps;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;

import static android.content.Context.LOCATION_SERVICE;

public class GpsLocationReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction() != null && intent.getAction().matches("android.location.PROVIDERS_CHANGED")) {
            LocationManager locationManager = (LocationManager) context.getSystemService(LOCATION_SERVICE);
            if (locationManager != null ) {
                context.sendBroadcast(new Intent("GPS_RECEIVER")
                .putExtra("isGPSEnabled",locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)));
            }
        }
    }
}
