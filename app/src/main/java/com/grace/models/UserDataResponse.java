package com.grace.models;

import com.google.gson.annotations.SerializedName;

public class UserDataResponse {


    /**
     * user_id : 5b7d043be03b8625ad582372
     * role : 2
     * email : f@yopmail.com
     * phone : 8754213690
     * full_name : test user
     * salon_image :
     * expertise : 5b6bd775e03b8616f001dd54,5b6bd890e03b8617541310e4
     * salon_name : fresh salon
     * latitude : 0
     * longitude : 0
     * address :
     */

    @SerializedName("user_id")
    private String userId;
    @SerializedName("role")
    private String role;
    @SerializedName("email")
    private String email;
    @SerializedName("phone")
    private String phone;
    @SerializedName("full_name")
    private String fullName;
    @SerializedName("salon_image")
    private String salonImage;
    @SerializedName("expertise")
    private String expertise;
    @SerializedName("salon_name")
    private String salonName;
    @SerializedName("latitude")
    private double latitude;
    @SerializedName("longitude")
    private double longitude;
    @SerializedName("address")
    private String address;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getSalonImage() {
        return salonImage;
    }

    public void setSalonImage(String salonImage) {
        this.salonImage = salonImage;
    }

    public String getExpertise() {
        return expertise;
    }

    public void setExpertise(String expertise) {
        this.expertise = expertise;
    }

    public String getSalonName() {
        return salonName;
    }

    public void setSalonName(String salonName) {
        this.salonName = salonName;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
