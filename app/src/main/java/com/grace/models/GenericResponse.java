package com.grace.models;

public class GenericResponse {

    /**
     * status : 201
     * Message : User not exist
     */

    private int status;
    private String Message;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }
}
