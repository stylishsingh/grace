package com.grace.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ServiceProviderListResponse extends GenericResponse {

    @SerializedName("data")
    private List<DataBean> data;

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Parcelable {
        /**
         * user_id : 5b69652de03b865e03692b32
         * email : devtester161@gmail.com
         * phone : 1234567890
         * full_name : test user
         * latitude : 30.704649
         * longitude : 76.717873
         * address :
         * salon_info : {"salon_id":"5b98b3f630724182d6be8c49","name":"fresh salon","image":"http://graceapp.co.in/serviceprovider/images/5b69652de03b865e03692b32.png","rating":0}
         * expertise : [{"expertise_id":"5b6bd775e03b8616f001dd54","name":"Beauty and SPA"},{"expertise_id":"5b6bd890e03b8617541310e4","name":"Latest Trend"}]
         */


        @SerializedName("user_id")
        private String userId;
        @SerializedName("email")
        private String email;
        @SerializedName("phone")
        private String phone;
        @SerializedName("full_name")
        private String fullName;
        @SerializedName("latitude")
        private double latitude;
        @SerializedName("longitude")
        private double longitude;
        @SerializedName("address")
        private String address;
        @SerializedName("salon_info")
        private SalonInfoBean salonInfo;
        @SerializedName("distance")
        private String distance;
        @SerializedName("time")
        private int time;
        @SerializedName("expertise")
        private List<ExpertiseBean> expertise;

        public int getTime() {
            return time;
        }

        public void setTime(int time) {
            this.time = time;
        }

        public String getDistance() {
            return distance;
        }

        public void setDistance(String distance) {
            this.distance = distance;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getFullName() {
            return fullName;
        }

        public void setFullName(String fullName) {
            this.fullName = fullName;
        }

        public double getLatitude() {
            return latitude;
        }

        public void setLatitude(double latitude) {
            this.latitude = latitude;
        }

        public double getLongitude() {
            return longitude;
        }

        public void setLongitude(double longitude) {
            this.longitude = longitude;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public SalonInfoBean getSalonInfo() {
            return salonInfo;
        }

        public void setSalonInfo(SalonInfoBean salonInfo) {
            this.salonInfo = salonInfo;
        }

        public List<ExpertiseBean> getExpertise() {
            return expertise;
        }

        public void setExpertise(List<ExpertiseBean> expertise) {
            this.expertise = expertise;
        }

        public static class SalonInfoBean implements Parcelable {
            /**
             * salon_id : 5b98b3f630724182d6be8c49
             * name : fresh salon
             * image : http://graceapp.co.in/serviceprovider/images/5b69652de03b865e03692b32.png
             * rating : 0
             */

            @SerializedName("salon_id")
            private String salonId;
            @SerializedName("name")
            private String name;
            @SerializedName("image")
            private String image;
            @SerializedName("rating")
            private float rating;

            public String getSalonId() {
                return salonId;
            }

            public void setSalonId(String salonId) {
                this.salonId = salonId;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }

            public float getRating() {
                return rating;
            }

            public void setRating(float rating) {
                this.rating = rating;
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(this.salonId);
                dest.writeString(this.name);
                dest.writeString(this.image);
                dest.writeFloat(this.rating);
            }

            public SalonInfoBean() {
            }

            protected SalonInfoBean(Parcel in) {
                this.salonId = in.readString();
                this.name = in.readString();
                this.image = in.readString();
                this.rating = in.readInt();
            }

            public static final Parcelable.Creator<SalonInfoBean> CREATOR = new Parcelable.Creator<SalonInfoBean>() {
                @Override
                public SalonInfoBean createFromParcel(Parcel source) {
                    return new SalonInfoBean(source);
                }

                @Override
                public SalonInfoBean[] newArray(int size) {
                    return new SalonInfoBean[size];
                }
            };
        }

        public static class ExpertiseBean implements Parcelable {
            /**
             * expertise_id : 5b6bd775e03b8616f001dd54
             * name : Beauty and SPA
             */

            @SerializedName("expertise_id")
            private String expertiseId;
            @SerializedName("name")
            private String name;

            public String getExpertiseId() {
                return expertiseId;
            }

            public void setExpertiseId(String expertiseId) {
                this.expertiseId = expertiseId;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(this.expertiseId);
                dest.writeString(this.name);
            }

            public ExpertiseBean() {
            }

            protected ExpertiseBean(Parcel in) {
                this.expertiseId = in.readString();
                this.name = in.readString();
            }

            public static final Parcelable.Creator<ExpertiseBean> CREATOR = new Parcelable.Creator<ExpertiseBean>() {
                @Override
                public ExpertiseBean createFromParcel(Parcel source) {
                    return new ExpertiseBean(source);
                }

                @Override
                public ExpertiseBean[] newArray(int size) {
                    return new ExpertiseBean[size];
                }
            };
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.time);
            dest.writeString(this.distance);
            dest.writeString(this.userId);
            dest.writeString(this.email);
            dest.writeString(this.phone);
            dest.writeString(this.fullName);
            dest.writeDouble(this.latitude);
            dest.writeDouble(this.longitude);
            dest.writeString(this.address);
            dest.writeParcelable(this.salonInfo, flags);
            dest.writeTypedList(this.expertise);
        }

        public DataBean() {
        }

        protected DataBean(Parcel in) {
            this.distance = in.readString();
            this.time = in.readInt();
            this.userId = in.readString();
            this.email = in.readString();
            this.phone = in.readString();
            this.fullName = in.readString();
            this.latitude = in.readDouble();
            this.longitude = in.readDouble();
            this.address = in.readString();
            this.salonInfo = in.readParcelable(SalonInfoBean.class.getClassLoader());
            this.expertise = in.createTypedArrayList(ExpertiseBean.CREATOR);
        }

        public static final Parcelable.Creator<DataBean> CREATOR = new Parcelable.Creator<DataBean>() {
            @Override
            public DataBean createFromParcel(Parcel source) {
                return new DataBean(source);
            }

            @Override
            public DataBean[] newArray(int size) {
                return new DataBean[size];
            }
        };
    }
}
