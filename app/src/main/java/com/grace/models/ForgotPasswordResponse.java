package com.grace.models;

import com.google.gson.annotations.SerializedName;

public class ForgotPasswordResponse extends GenericResponse {


    /**
     * data : {"user_id":"5b7b98e0e03b863abd3a8832","email":"developertester478@gmail.com"}
     */

    @SerializedName("data")
    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * user_id : 5b7b98e0e03b863abd3a8832
         * email : developertester478@gmail.com
         */

        @SerializedName("user_id")
        private String userId;
        @SerializedName("email")
        private String email;

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }
    }
}
