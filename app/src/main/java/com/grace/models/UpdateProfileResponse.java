package com.grace.models;

import java.util.List;

public class UpdateProfileResponse extends GenericResponse{


    private List<UserDataResponse> data;

    public List<UserDataResponse> getData() {
        return data;
    }

    public void setData(List<UserDataResponse> data) {
        this.data = data;
    }

}
