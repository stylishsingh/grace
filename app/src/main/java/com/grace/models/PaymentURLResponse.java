package com.grace.models;

import com.google.gson.annotations.SerializedName;

public class PaymentURLResponse extends GenericResponse{


    /**
     * redirectUrl : https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token=EC-08F243984E837750R
     */

    @SerializedName("redirectUrl")
    private String redirectUrl;

    public String getRedirectUrl() {
        return redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }
}
