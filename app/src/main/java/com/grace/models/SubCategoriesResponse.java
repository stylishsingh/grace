package com.grace.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SubCategoriesResponse extends GenericResponse {


    @SerializedName("data")
    private List<DataBean> data;

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * subcategory_id : 5b6bd890e03b8617541310e2
         * name : Facial
         * amount : {"men":"$100","women":"$200","kids":"$50"}
         */

        @SerializedName("subcategory_id")
        private String subcategoryId;
        @SerializedName("name")
        private String name;
        @SerializedName("amount")
        private AmountBean amount;

        public String getSubcategoryId() {
            return subcategoryId;
        }

        public void setSubcategoryId(String subcategoryId) {
            this.subcategoryId = subcategoryId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public AmountBean getAmount() {
            return amount;
        }

        public void setAmount(AmountBean amount) {
            this.amount = amount;
        }

        public static class AmountBean {
            /**
             * men : $100
             * women : $200
             * kids : $50
             */

            @SerializedName("men")
            private String men;
            @SerializedName("women")
            private String women;
            @SerializedName("kids")
            private String kids;

            public String getMen() {
                return men;
            }

            public void setMen(String men) {
                this.men = men;
            }

            public String getWomen() {
                return women;
            }

            public void setWomen(String women) {
                this.women = women;
            }

            public String getKids() {
                return kids;
            }

            public void setKids(String kids) {
                this.kids = kids;
            }
        }
    }
}
