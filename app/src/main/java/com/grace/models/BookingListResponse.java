package com.grace.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BookingListResponse extends GenericResponse {


    @SerializedName("data")
    private List<DataBean> data;

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Parcelable {
        /**
         * booking_id : 5bd96e05e03b866d8d758b52
         * user_info : {"user_id":"5bbbf4b1e03b862b492a4262","name":"custmer one","image":"","phone":"2145639870","email":"customer1@yopmail.com","address":"Phase 8, Industrial Area, Sahibzada Ajit Singh Nagar, Punjab 140308, India"}
         * provider_info : {"service_provider_id":"5bbbf51de03b862b7d036ae2","salon_id":"5bbbf51de03b862b7d036ae3","name":"provider one","image":"","phone":"8745321909","email":"provider1@gmail.com","address":"Unnamed Road, Phase 8, Industrial Area, Sector 73, Sahibzada Ajit Singh Nagar, Punjab 140308, India"}
         * time : 30/11/2018 5:43
         * total_amount : 70
         * latitude : 30.7533175
         * longitude : 76.62218359375
         * paymentstatus : 0
         * job_status : 2
         * men : 1
         * women : 0
         * kids : 0
         * rating : 5.0
         * comment : chuvjk
         * accept : 1
         * booking_address : Flat no 1005/1, Ground Floor, Ludhiana Road, Kharar, Sahibzada Ajit Singh Nagar, Punjab 140301, India
         */

        @SerializedName("booking_id")
        private String bookingId;
        @SerializedName("user_info")
        private UserInfoBean userInfo;
        @SerializedName("provider_info")
        private ProviderInfoBean providerInfo;
        @SerializedName("time")
        private String time;
        @SerializedName("total_amount")
        private String totalAmount;
        @SerializedName("latitude")
        private double latitude;
        @SerializedName("longitude")
        private double longitude;
        @SerializedName("paymentstatus")
        private int paymentstatus;
        @SerializedName("job_status")
        private int jobStatus;
        @SerializedName("men")
        private int men;
        @SerializedName("women")
        private int women;
        @SerializedName("kids")
        private int kids;
        @SerializedName("rating")
        private String rating;
        @SerializedName("comment")
        private String comment;
        @SerializedName("accept")
        private int accept;
        @SerializedName("booking_address")
        private String bookingAddress;

        public String getBookingId() {
            return bookingId;
        }

        public void setBookingId(String bookingId) {
            this.bookingId = bookingId;
        }

        public UserInfoBean getUserInfo() {
            return userInfo;
        }

        public void setUserInfo(UserInfoBean userInfo) {
            this.userInfo = userInfo;
        }

        public ProviderInfoBean getProviderInfo() {
            return providerInfo;
        }

        public void setProviderInfo(ProviderInfoBean providerInfo) {
            this.providerInfo = providerInfo;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public String getTotalAmount() {
            return totalAmount;
        }

        public void setTotalAmount(String totalAmount) {
            this.totalAmount = totalAmount;
        }

        public double getLatitude() {
            return latitude;
        }

        public void setLatitude(double latitude) {
            this.latitude = latitude;
        }

        public double getLongitude() {
            return longitude;
        }

        public void setLongitude(double longitude) {
            this.longitude = longitude;
        }

        public int getPaymentstatus() {
            return paymentstatus;
        }

        public void setPaymentstatus(int paymentstatus) {
            this.paymentstatus = paymentstatus;
        }

        public int getJobStatus() {
            return jobStatus;
        }

        public void setJobStatus(int jobStatus) {
            this.jobStatus = jobStatus;
        }

        public int getMen() {
            return men;
        }

        public void setMen(int men) {
            this.men = men;
        }

        public int getWomen() {
            return women;
        }

        public void setWomen(int women) {
            this.women = women;
        }

        public int getKids() {
            return kids;
        }

        public void setKids(int kids) {
            this.kids = kids;
        }

        public String getRating() {
            return rating;
        }

        public void setRating(String rating) {
            this.rating = rating;
        }

        public String getComment() {
            return comment;
        }

        public void setComment(String comment) {
            this.comment = comment;
        }

        public int getAccept() {
            return accept;
        }

        public void setAccept(int accept) {
            this.accept = accept;
        }

        public String getBookingAddress() {
            return bookingAddress;
        }

        public void setBookingAddress(String bookingAddress) {
            this.bookingAddress = bookingAddress;
        }

        public static class UserInfoBean implements Parcelable {
            /**
             * user_id : 5bbbf4b1e03b862b492a4262
             * name : custmer one
             * image :
             * phone : 2145639870
             * email : customer1@yopmail.com
             * address : Phase 8, Industrial Area, Sahibzada Ajit Singh Nagar, Punjab 140308, India
             */

            @SerializedName("user_id")
            private String userId;
            @SerializedName("name")
            private String name;
            @SerializedName("image")
            private String image;
            @SerializedName("phone")
            private String phone;
            @SerializedName("email")
            private String email;
            @SerializedName("address")
            private String address;

            public String getUserId() {
                return userId;
            }

            public void setUserId(String userId) {
                this.userId = userId;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }

            public String getPhone() {
                return phone;
            }

            public void setPhone(String phone) {
                this.phone = phone;
            }

            public String getEmail() {
                return email;
            }

            public void setEmail(String email) {
                this.email = email;
            }

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(this.userId);
                dest.writeString(this.name);
                dest.writeString(this.image);
                dest.writeString(this.phone);
                dest.writeString(this.email);
                dest.writeString(this.address);
            }

            public UserInfoBean() {
            }

            protected UserInfoBean(Parcel in) {
                this.userId = in.readString();
                this.name = in.readString();
                this.image = in.readString();
                this.phone = in.readString();
                this.email = in.readString();
                this.address = in.readString();
            }

            public static final Parcelable.Creator<UserInfoBean> CREATOR = new Parcelable.Creator<UserInfoBean>() {
                @Override
                public UserInfoBean createFromParcel(Parcel source) {
                    return new UserInfoBean(source);
                }

                @Override
                public UserInfoBean[] newArray(int size) {
                    return new UserInfoBean[size];
                }
            };
        }

        public static class ProviderInfoBean implements Parcelable {
            /**
             * service_provider_id : 5bbbf51de03b862b7d036ae2
             * salon_id : 5bbbf51de03b862b7d036ae3
             * name : provider one
             * image :
             * phone : 8745321909
             * email : provider1@gmail.com
             * address : Unnamed Road, Phase 8, Industrial Area, Sector 73, Sahibzada Ajit Singh Nagar, Punjab 140308, India
             */

            @SerializedName("service_provider_id")
            private String serviceProviderId;
            @SerializedName("salon_id")
            private String salonId;
            @SerializedName("name")
            private String name;
            @SerializedName("image")
            private String image;
            @SerializedName("phone")
            private String phone;
            @SerializedName("email")
            private String email;
            @SerializedName("address")
            private String address;

            public String getServiceProviderId() {
                return serviceProviderId;
            }

            public void setServiceProviderId(String serviceProviderId) {
                this.serviceProviderId = serviceProviderId;
            }

            public String getSalonId() {
                return salonId;
            }

            public void setSalonId(String salonId) {
                this.salonId = salonId;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }

            public String getPhone() {
                return phone;
            }

            public void setPhone(String phone) {
                this.phone = phone;
            }

            public String getEmail() {
                return email;
            }

            public void setEmail(String email) {
                this.email = email;
            }

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(this.serviceProviderId);
                dest.writeString(this.salonId);
                dest.writeString(this.name);
                dest.writeString(this.image);
                dest.writeString(this.phone);
                dest.writeString(this.email);
                dest.writeString(this.address);
            }

            public ProviderInfoBean() {
            }

            protected ProviderInfoBean(Parcel in) {
                this.serviceProviderId = in.readString();
                this.salonId = in.readString();
                this.name = in.readString();
                this.image = in.readString();
                this.phone = in.readString();
                this.email = in.readString();
                this.address = in.readString();
            }

            public static final Parcelable.Creator<ProviderInfoBean> CREATOR = new Parcelable.Creator<ProviderInfoBean>() {
                @Override
                public ProviderInfoBean createFromParcel(Parcel source) {
                    return new ProviderInfoBean(source);
                }

                @Override
                public ProviderInfoBean[] newArray(int size) {
                    return new ProviderInfoBean[size];
                }
            };
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.bookingId);
            dest.writeParcelable(this.userInfo, flags);
            dest.writeParcelable(this.providerInfo, flags);
            dest.writeString(this.time);
            dest.writeString(this.totalAmount);
            dest.writeDouble(this.latitude);
            dest.writeDouble(this.longitude);
            dest.writeInt(this.paymentstatus);
            dest.writeInt(this.jobStatus);
            dest.writeInt(this.men);
            dest.writeInt(this.women);
            dest.writeInt(this.kids);
            dest.writeString(this.rating);
            dest.writeString(this.comment);
            dest.writeInt(this.accept);
            dest.writeString(this.bookingAddress);
        }

        public DataBean() {
        }

        protected DataBean(Parcel in) {
            this.bookingId = in.readString();
            this.userInfo = in.readParcelable(UserInfoBean.class.getClassLoader());
            this.providerInfo = in.readParcelable(ProviderInfoBean.class.getClassLoader());
            this.time = in.readString();
            this.totalAmount = in.readString();
            this.latitude = in.readDouble();
            this.longitude = in.readDouble();
            this.paymentstatus = in.readInt();
            this.jobStatus = in.readInt();
            this.men = in.readInt();
            this.women = in.readInt();
            this.kids = in.readInt();
            this.rating = in.readString();
            this.comment = in.readString();
            this.accept = in.readInt();
            this.bookingAddress = in.readString();
        }

        public static final Parcelable.Creator<DataBean> CREATOR = new Parcelable.Creator<DataBean>() {
            @Override
            public DataBean createFromParcel(Parcel source) {
                return new DataBean(source);
            }

            @Override
            public DataBean[] newArray(int size) {
                return new DataBean[size];
            }
        };
    }
}
