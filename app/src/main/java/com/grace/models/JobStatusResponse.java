package com.grace.models;

import com.google.gson.annotations.SerializedName;

public class JobStatusResponse extends GenericResponse {


    /**
     * data : {"booking_id":"5bdad30ce03b86596f09e732","name":"provider one","email":"provider1@gmail.com","phone":"8745321909","address":"Unnamed Road, Phase 8, Industrial Area, Sector 73, Sahibzada Ajit Singh Nagar, Punjab 140308, India","job_status":1}
     */

    @SerializedName("data")
    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * booking_id : 5bdad30ce03b86596f09e732
         * name : provider one
         * email : provider1@gmail.com
         * phone : 8745321909
         * address : Unnamed Road, Phase 8, Industrial Area, Sector 73, Sahibzada Ajit Singh Nagar, Punjab 140308, India
         * job_status : 1
         */

        @SerializedName("booking_id")
        private String bookingId;
        @SerializedName("name")
        private String name;
        @SerializedName("email")
        private String email;
        @SerializedName("phone")
        private String phone;
        @SerializedName("address")
        private String address;
        @SerializedName("job_status")
        private int jobStatus;

        public String getBookingId() {
            return bookingId;
        }

        public void setBookingId(String bookingId) {
            this.bookingId = bookingId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public int getJobStatus() {
            return jobStatus;
        }

        public void setJobStatus(int jobStatus) {
            this.jobStatus = jobStatus;
        }
    }
}
