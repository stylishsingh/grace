package com.grace.firebase;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.grace.R;
import com.grace.ui.common.HolderActivity;
import com.grace.utils.Constants;
import com.grace.utils.Prefs;
import com.grace.utils.Utils;

import java.util.Map;

public class MyFirebaseMessagingService extends FirebaseMessagingService implements Constants {

    private static final String TAG = "MyFirebaseMsgService";
    public static int notificationID = 0;
    public static NotificationManager notificationManager;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        if (remoteMessage.getNotification() != null) {
            System.out.println("Remote message-->" + remoteMessage.getData());
            Map<String, String> remoteBody = remoteMessage.getData();
            Intent intent;
            switch (remoteBody.get("customparam")) {
                case "1":
                    // Booking
                    if (Prefs.getString(PARAMS.ROLE, "").equalsIgnoreCase("2")) {
                        intent = new Intent(getApplicationContext(), HolderActivity.class);
                        intent.putExtra(NAVIGATE_SCREENS.SCREEN, NAVIGATE_SCREENS.BOOKING_DETAIL_SCREEN);
                        intent.putExtra(PARAMS.BOOKING_ID, remoteBody.get(PARAMS.BOOKING_ID));
                        generateNotification(remoteMessage.getNotification().getBody() + "\n"
                                + remoteBody.get(BOOKING_ADDRESS), remoteBody, intent);
                        sendBroadcast(new Intent(API_CALLS.BOOKING_LIST));
                    }
                    break;
                case "2":
                    // Accept
                    if (Prefs.getString(PARAMS.ROLE, "").equalsIgnoreCase("1")) {
                        intent = new Intent(getApplicationContext(), HolderActivity.class);
                        intent.putExtra(NAVIGATE_SCREENS.SCREEN, NAVIGATE_SCREENS.BOOKING_DETAIL_SCREEN);
                        intent.putExtra(PARAMS.BOOKING_ID, remoteBody.get(PARAMS.BOOKING_ID));
                        generateNotification(remoteMessage.getNotification().getBody(), remoteBody, intent);
                    }
                    break;
                case "3":
                    // Payment
                    if (Prefs.getString(PARAMS.ROLE, "").equalsIgnoreCase("2")) {
                        intent = new Intent(getApplicationContext(), HolderActivity.class);
                        intent.putExtra(NAVIGATE_SCREENS.SCREEN, NAVIGATE_SCREENS.BOOKING_DETAIL_SCREEN);
                        intent.putExtra(PARAMS.BOOKING_ID, remoteBody.get(PARAMS.BOOKING_ID));
                        generateNotification(remoteMessage.getNotification().getBody(), remoteBody, intent);
                    }
                    break;
                case "4":
                    // Job Start
                    if (Prefs.getString(PARAMS.ROLE, "").equalsIgnoreCase("1")) {
                        intent = new Intent(getApplicationContext(), HolderActivity.class);
                        intent.putExtra(NAVIGATE_SCREENS.SCREEN, NAVIGATE_SCREENS.BOOKING_DETAIL_SCREEN);
                        intent.putExtra(PARAMS.BOOKING_ID, remoteBody.get(PARAMS.BOOKING_ID));
                        generateNotification(remoteMessage.getNotification().getBody(), remoteBody, intent);
                    }
                    break;
                case "5":
                    // Job Finish
                    if (Prefs.getString(PARAMS.ROLE, "").equalsIgnoreCase("1")) {
                        intent = new Intent(getApplicationContext(), HolderActivity.class);
                        intent.putExtra(NAVIGATE_SCREENS.SCREEN, NAVIGATE_SCREENS.BOOKING_DETAIL_SCREEN);
                        intent.putExtra(PARAMS.BOOKING_ID, remoteBody.get(PARAMS.BOOKING_ID));
                        generateNotification(remoteMessage.getNotification().getBody(), remoteBody, intent);
                    }
                    break;
                case "6":
                    // Feedback
                    if (Prefs.getString(PARAMS.ROLE, "").equalsIgnoreCase("2")) {
                        intent = new Intent(getApplicationContext(), HolderActivity.class);
                        intent.putExtra(NAVIGATE_SCREENS.SCREEN, NAVIGATE_SCREENS.BOOKING_DETAIL_SCREEN);
                        intent.putExtra(PARAMS.BOOKING_ID, remoteBody.get(PARAMS.BOOKING_ID));
                        generateNotification(remoteMessage.getNotification().getBody(), remoteBody, intent);
                    }
                    break;
                default:
                    System.out.println("Default");


            }

            sendBroadcast(new Intent(API_CALLS.BOOKING_DETAILS)
                    .putExtra(PARAMS.BOOKING_ID, remoteBody.get(PARAMS.BOOKING_ID)));

        }
    }


    //This method is only generating push notification
    //It is same as we did in earlier posts
    private void generateNotification(String messageBody, Map<String, String> remoteBody, Intent intent) {
// The id of the channel.
        String id = "my_channel_01";
        try {
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                    PendingIntent.FLAG_UPDATE_CURRENT);
            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

            NotificationCompat.Builder notification = new NotificationCompat.Builder(this, id)
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(messageBody))
                    .setSmallIcon(getNotificationIcon())
                    .setContentTitle(getString(R.string.app_name))
                    .setContentText(messageBody)
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setColor(ContextCompat.getColor(this, R.color.colorPrimaryDark))
                    .setDefaults(NotificationCompat.DEFAULT_ALL)
                    .setContentIntent(pendingIntent);
            if (Utils.getInstance().isEqualLollipop()) {
                notification.setFullScreenIntent(pendingIntent, false);
            }

            System.out.println("MyFirebaseMessagingService.generateNotification----" + notificationID);

            notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            if (notificationManager != null) {

                System.out.println("Notification Manager 1---->" + notificationManager);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    int importance = NotificationManager.IMPORTANCE_HIGH;
                    NotificationChannel mChannel = new NotificationChannel(id, "name", importance);
                    notificationManager.createNotificationChannel(mChannel);
                }

                System.out.println("Notification Manager 2---->" + notificationManager);

                notificationManager.notify(notificationID, notification.build());
                notificationID++;
            }

            System.out.println("Notification Manager 3---->" + notificationManager);

//            notificationManager.cancel(notificationID);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // function to get the notification icon based on version lollipop or not
    private int getNotificationIcon() {
        boolean whiteIcon = (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP);
        return whiteIcon ? R.drawable.ic_launcher_white : R.mipmap.ic_launcher;
    }


}
