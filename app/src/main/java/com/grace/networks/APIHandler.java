/*
 *
 *
 *  * Copyright © 2016, Mobilyte Inc. and/or its affiliates. All rights reserved.
 *  *
 *  * Redistribution and use in source and binary forms, with or without
 *  * modification, are permitted provided that the following conditions are met:
 *  *
 *  * - Redistributions of source code must retain the above copyright
 *  *    notice, this list of conditions and the following disclaimer.
 *  *
 *  * - Redistributions in binary form must reproduce the above copyright
 *  * notice, this list of conditions and the following disclaimer in the
 *  * documentation and/or other materials provided with the distribution.
 *
 * /
 */
package com.grace.networks;


import com.grace.models.BookingListResponse;
import com.grace.models.ExpertiseResponse;
import com.grace.models.ForgotPasswordResponse;
import com.grace.models.GenericResponse;
import com.grace.models.JobStatusResponse;
import com.grace.models.LoginResponse;
import com.grace.models.PaymentURLResponse;
import com.grace.models.ServiceProviderListResponse;
import com.grace.models.SignUpResponse;
import com.grace.models.SubCategoriesResponse;
import com.grace.models.UpdateProfileResponse;
import com.grace.utils.Constants;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * @author Amanpal Singh.
 */
public class APIHandler implements APIResponseInterface, Constants.API_CALLS {
    private APIResponseInterface.OnCallableResponse listener;


    public APIHandler(APIResponseInterface.OnCallableResponse listener) {
        this.listener = listener;
    }

    public void login(Map<String, String> params, String apiName) {
        Call<LoginResponse> call = RetrofitClient.getRetrofitClient().login(params);
        callable(listener, call, apiName);
    }

    public void signUp(Map<String, String> params, String apiName) {
        Call<SignUpResponse> call = RetrofitClient.getRetrofitClient().signUp(params);
        callable(listener, call, apiName);
    }

    public void updateProfileWithPic(Map<String, String> params, String salonImage, String apiName) {
        RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), new File(salonImage));
        MultipartBody.Part body = MultipartBody.Part.createFormData("salon_image", "user_profile.jpg", requestBody);

        HashMap<String, RequestBody> bodyParam = new HashMap<>();
        for (Map.Entry<String, String> obj : params.entrySet()) {
            bodyParam.put(obj.getKey(), RequestBody.create(MediaType.parse("multipart/form-data"), obj.getValue()));
        }
        Call<UpdateProfileResponse> call = RetrofitClient.getRetrofitClient().updateProfileWithPic(bodyParam, body);
        callable(listener, call, apiName);
    }

    public void updateProfile(Map<String, String> params, String apiName) {
        Call<UpdateProfileResponse> call = RetrofitClient.getRetrofitClient().updateProfile(params);
        callable(listener, call, apiName);
    }

    public void getExpertise(String apiName) {
        Call<ExpertiseResponse> call = RetrofitClient.getRetrofitClient().getExpertise();
        callable(listener, call, apiName);
    }

    public void getProviderList(Map<String, String> params, String apiName) {
        Call<ServiceProviderListResponse> call = RetrofitClient.getRetrofitClient().getServiceProviderList(params);
        callable(listener, call, apiName);
    }

    public void getSubCategories(Map<String, String> params, String apiName) {
        Call<SubCategoriesResponse> call = RetrofitClient.getRetrofitClient().getSubCategories(params);
        callable(listener, call, apiName);
    }

    public void bookAppointment(Map<String, String> params, String apiName) {
        Call<GenericResponse> call = RetrofitClient.getRetrofitClient().bookAppointment(params);
        callable(listener, call, apiName);
    }

    public void getBookingList(Map<String, String> params, String apiName) {
        Call<BookingListResponse> call = RetrofitClient.getRetrofitClient().getBookingList(params);
        callable(listener, call, apiName);
    }

    public void getPaymentURL(Map<String, String> params, String apiName) {
        Call<PaymentURLResponse> call = RetrofitClient.getRetrofitClient().getPaymentURL(params);
        callable(listener, call, apiName);
    }

    public void acceptBooking(Map<String, String> params, String apiName) {
        Call<GenericResponse> call = RetrofitClient.getRetrofitClient().acceptBooking(params);
        callable(listener, call, apiName);
    }

    public void sendFeedback(Map<String, String> params, String apiName) {
        Call<GenericResponse> call = RetrofitClient.getRetrofitClient().sendFeedback(params);
        callable(listener, call, apiName);
    }

    public void forgotPassword(Map<String, String> params, String apiName) {
        Call<ForgotPasswordResponse> call = RetrofitClient.getRetrofitClient().forgotPassword(params);
        callable(listener, call, apiName);
    }

    public void resetPassword(Map<String, String> params, String apiName) {
        Call<GenericResponse> call = RetrofitClient.getRetrofitClient().resetPassword(params);
        callable(listener, call, apiName);
    }

    public void jobStatusChanged(Map<String, String> params, String apiName) {
        Call<JobStatusResponse> call = RetrofitClient.getRetrofitClient().jobStatusChanged(params);
        callable(listener, call, apiName);
    }

    public void getBookingDetails(Map<String, String> params, String apiName) {
        Call<BookingListResponse> call = RetrofitClient.getRetrofitClient().getBookingDetails(params);
        callable(listener, call, apiName);
    }


    @Override
    public <T> void callable(OnCallableResponse onCallableResponse, Call<T> call, final String api) {
        call.enqueue(new Callback<T>() {
            @Override
            public void onResponse(Call<T> call, Response<T> response) {
                listener.onSuccess(response, api);
            }

            @Override
            public void onFailure(Call<T> call, Throwable t) {

                listener.onFailure(t, api);
            }
        });
    }


}
