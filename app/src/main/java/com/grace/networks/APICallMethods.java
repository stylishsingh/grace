package com.grace.networks;


import com.grace.models.BookingListResponse;
import com.grace.models.ExpertiseResponse;
import com.grace.models.ForgotPasswordResponse;
import com.grace.models.GenericResponse;
import com.grace.models.JobStatusResponse;
import com.grace.models.LoginResponse;
import com.grace.models.PaymentURLResponse;
import com.grace.models.ServiceProviderListResponse;
import com.grace.models.SignUpResponse;
import com.grace.models.SubCategoriesResponse;
import com.grace.models.UpdateProfileResponse;
import com.grace.utils.Constants;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;

/**
 * @author Amanpal Singh.
 */

public interface APICallMethods {

    @FormUrlEncoded
    @POST(Constants.API_CALLS.LOGIN)
    Call<LoginResponse> login(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST(Constants.API_CALLS.SIGN_UP)
    Call<SignUpResponse> signUp(@FieldMap Map<String, String> params);

    @Multipart
    @POST(Constants.API_CALLS.UPDATE_PROFILE)
    Call<UpdateProfileResponse> updateProfileWithPic(@PartMap Map<String, RequestBody> options, @Part MultipartBody.Part profile_pic);

    @FormUrlEncoded
    @POST(Constants.API_CALLS.UPDATE_PROFILE)
    Call<UpdateProfileResponse> updateProfile(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST(Constants.API_CALLS.SERVICE_PROVIDER_LIST)
    Call<ServiceProviderListResponse> getServiceProviderList(@FieldMap Map<String, String> params);

    @GET(Constants.API_CALLS.EXPERTISE_LIST)
    Call<ExpertiseResponse> getExpertise();

    @FormUrlEncoded
    @POST(Constants.API_CALLS.SUBCATEGORIES_LIST)
    Call<SubCategoriesResponse> getSubCategories(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST(Constants.API_CALLS.BOOK_APPOINTMENT)
    Call<GenericResponse> bookAppointment(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST(Constants.API_CALLS.BOOKING_LIST)
    Call<BookingListResponse> getBookingList(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST(Constants.API_CALLS.BOOKING_PAYMENT_PROCESS)
    Call<PaymentURLResponse> getPaymentURL(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST(Constants.API_CALLS.ACCEPT_BOOKING)
    Call<GenericResponse> acceptBooking(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST(Constants.API_CALLS.FEEDBACk)
    Call<GenericResponse> sendFeedback(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST(Constants.API_CALLS.FORGOT_PASSWORD)
    Call<ForgotPasswordResponse> forgotPassword(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST(Constants.API_CALLS.RESET_PASSWORD)
    Call<GenericResponse> resetPassword(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST(Constants.API_CALLS.JOB_STATUS_CHANGED)
    Call<JobStatusResponse> jobStatusChanged(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST(Constants.API_CALLS.BOOKING_DETAILS)
    Call<BookingListResponse> getBookingDetails(@FieldMap Map<String, String> params);

}
